<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['ci_cms_template_dir'] = ""; // you need this line
$config['ci_cms_template_dir_public'] = $config['ci_cms_template_dir'] . "templates/";
$config['ci_cms_template_dir_admin'] = $config['ci_cms_template_dir'] . "admin/";
$config['ci_cms_template_dir_admin_templates'] = $config['ci_cms_template_dir'] . "admin/templates/";
$config['ci_cms_template_dir_welcome'] = $config['ci_cms_template_dir'] . "welcome/";
