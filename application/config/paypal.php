<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Paypal IPN Class
// ------------------------------------------------------------------------

// If (and where) to log ipn to file
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';

$config['paypal_lib_ipn_log'] = TRUE;

// Where are the buttons located at 
$config['paypal_lib_button_path'] = 'buttons';

// What is the default currency?
$config['paypal_lib_currency_code'] = 'USD';

// test paypal url
$config['paypal_url_sandbox'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

//paypal account
$config['paypal_email'] = 'asif.u@allshoreresources.com';

// paypal urls
$config['return_url'] = base_url().'checkout/success';
// paypal express checkout return url
$config['success_express_checkout'] = base_url().'checkout/success_express_checkout';

// paypal cancel urls
$config['cancel_url'] = base_url().'checkout/cancel';

// paypal ipn url
$config['ipn_url'] = base_url().'checkout/ipn';

// paypal account detail 
$config['paypal_api_user'] = 'asif.u-facilitator_api1.allshoreresources.com';
$config['paypal_api_password'] = '3P4UBYSCGYX4NN5W';
$config['paypal_api_signature'] = 'ANd7Mn.xJs3p2JArXZ32CUvvhgmnAkXzvPb7hOq6qZLGVMot-SpiytAr';
$config['paypal_mode']	   = 'sandbox';




?>
