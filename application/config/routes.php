<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'home';

$route['404_override'] = '';
$route['admin/dashboard'] = 'admin/admin';
// PRODUCTS MODULE ROUTES
$route['admin/products'] = 'products';
$route['admin/products/add'] = 'products/add';
$route['admin/products/edit/(:num)'] = 'products/edit/$1';
$route['admin/products/delete/(:num)'] = 'products/delete/$1';
// CATEGORY MODULE ROUTES
$route['admin/categories'] = 'category';
$route['admin/category/add'] = 'category/add';
$route['admin/category/delete/(:num)'] = 'category/delete/$1';
$route['admin/category/edit/(:num)'] = 'category/edit/$1';

// Brand MODULE ROUTES
$route['admin/brands'] = 'products/brand';
$route['admin/brand/add'] = 'products/brand/add';
$route['admin/brand/delete/(:num)'] = 'products/brand/delete/$1';
$route['admin/brand/edit/(:num)'] = 'products/brand/edit/$1';

// Shop module Routes
$route['shop/product-category/(:any)'] = 'shop/category/$1'; // parent category
$route['shop/product-category/(:any)/(:any)'] = 'shop/category/$1/$2'; // parent and then child category

// Settings
$route['admin/settings'] = 'settings';
$route['admin/settings/payment'] = 'settings/payment';
$route['admin/settings/add'] = 'settings/add';



//
$route['translate_uri_dashes'] = FALSE;

