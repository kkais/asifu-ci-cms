<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('language','form'));
		$this->lang->load('auth');
		$this->load->model('user'); // custom user mode for login and registration
	}

	// redirect if needed, otherwise display the user list
	function index()
	{
		$data['page_title'] = "Login";
		$this->_view('auth/login_form.php',$data);
	}

	// log the user in
	function login()
	{
		$data['page_title'] = 'Login';
		$this->form_validation->set_rules( 'email', 'Email', 'required' );
		$this->form_validation->set_rules('password', 'Password', 'required');
		if( $this->form_validation->run() == true ){
			$remember = (bool) $this->input->post('remember');
			if($this->ion_auth->login_process($this->input->post('email'), $this->input->post('password'), $remember)){
				 redirect('/admin/dashboard');
			}else{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				$this->_view('auth/login_form');			
			}			
		}else{					
			$this->session->set_flashdata('message', 'Incorrect Login');						
			$this->_view('auth/login_form');
		}
		
		
	}
	public function logout() 
	{
        $this->ion_auth->logout();
		redirect('auth', 'refresh');
    }
	
	public function register()
	{
		if( !$this->permission->is_user_logged_in() ){
			$data['page_title'] = 'Register';
			$this->_view('auth/registration_form',$data);			
		}else{
			$this->_view('home');			
		}
	}
	
	// create a new user
	function create_user()
	{
		echo $fname = $this->input->post('first_name');
		exit;
		// validate input fields
		$this->form_validation->set_rules( 'first_name', 'First Name', 'required' );
		$this->form_validation->set_rules( 'last_name', 'Last Name', 'required' );
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[tbl_users.user_name]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');	
		if ($this->form_validation->run() == true){
			$fname = $this->input->post('first_name');
			$lname = $this->input->post('last_name');
			$email    = strtolower($this->input->post('email'));
            $username = $this->input->post('user_name');
            $password = $this->input->post('password');
			$password = $this->bcrypt->hash($password);
			$user_role = $this->input->post('user_role');
			$user_data = array(
							'user_name'	=> $username,
							'email'		=> $email,
							'user_name'	=> $username,
							'first_name'	=> $fname,
							'last_name'	=> $lname,
							'password'	=> $password,
						);
			if( $this->user->register_process( $user_data, $user_role )){
            	redirect("auth", 'refresh');
			}			
			
		}
		if( $this->form_validation->run() == FALSE ){
			$data['page_title'] = 'Register';
			$this->_view('auth/registration_form',$data);		
		}		
		
					
	}
	
	/**
	* This method handle validation of user registration form
	* @access public
	* @return response in json form
	* @author AsifU
	*/
	function validate_registration_form()
	{
		if($this->user->email_exists()){
			$data['error'] = TRUE;
			$data['error_msg'] = 'Email already exists, Please try another one';
		}else{
			$data['error'] = FALSE;
		}
		echo json_encode($data);
	}

}
