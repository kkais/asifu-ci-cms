<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://localhost/ci-cms/home
	 *	- or -
	 * 		http://localhost/ci-cms/
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	function __construct() 
	 {
        parent::__construct();
     }
	  
	public function index()
	{		
		$this->_view('home');
	}
}
