<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {
	 
	 
	 function __construct() {
        parent::__construct();		
     }

    public function index() {	
        //$data['page'] = $this->config->item('ci_cms_template_dir_admin') . "dashboard";		
		$data['total_users'] = $this->user->total_users();
        $this->_view('admin/dashboard.php', $data);		
    }
		
}