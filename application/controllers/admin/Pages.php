<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Admin_Controller {
	 
	
	 function __construct() 
	 {
        parent::__construct();
		$this->load->model(array('admin/page'));
		$this->load->helper('permission');
     }
	
	/**
	* This method list all pages
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
    public function index() 
	{
		// get all pages
		$pages = $this->page->get_pages();
		// save pages in array
		$data['pages'] = $pages;
		$this->_view('admin/pages_list.php', $data);			
    }
	
	/**
	* This method handle add page form
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function add() 
	{ 
		 // check post input fields
		 if ($this->input->post('name')) 
		 {
			// assigning post data to array
			$data['page_title'] = $this->input->post('name');
            $data['page_content'] = $this->input->post('description'); 
			$data['page_slug'] = $this->page->page_url( $this->input->post('name') );
			$data['page_author'] = $this->permission->get_user_id();
			$data['page_created'] = date('Y-m-d H:i:s');
			// add data
			$this->page->add_page($data);
			// redirection
            redirect('/admin/pages');
		 }
		// rendering view
		$this->_view('admin/add_page.php');			    
	}	
	
	/**
	* This method handle edit page form
	* @access public
	* @params page id
	* @return void()
	* @author AsifU
	*/
	public function edit( $page_id ) {
		 if ($this->input->post('name')) 
		 {
			// assigning post data to array
			$data['page_title'] = $this->input->post('name');
            $data['page_content'] = $this->input->post('description'); 
			// add data
			$this->page->update($data, $page_id);
			// redirection
            redirect('/admin/pages');
		 }		
		$page_data = $this->page->get_page( $page_id );		
		$data['page_data'] = $page_data;		
		$this->_view('admin/edit_page.php', $data);		
	}
	
	/**
	* This method handle delete page
	* @access public
	* @params page id
	* @return void()
	* @author AsifU
	*/
	public function delete($id) 
	{
        $this->page->delete($id);
        redirect('/admin/pages', 'refresh');
    }

}
/* End of file Pages.php */
/* Location: application/controllers/admin/Pages.php */