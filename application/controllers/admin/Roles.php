<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends Admin_Controller {
	 
	
	 function __construct() 
	 {
        parent::__construct();
		$this->load->model('admin/role');
		$this->load->helper('permission');
     }
	
	/**
	* This method list roles
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
    public function index() 
	{
		// get all roles
		$roles = $this->user->get_roles();
		// save roles in array
		$data['roles'] = $roles;
        $this->_view('admin/roles_list.php', $data);		
    }
	
	/**
	* This method add roles
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function add() 
	{ 
		 // check post input fields
		 if ($this->input->post('name')) 
		 {
			// assigning post data to array
			$data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description'); 
			$this->user->add_role($data);
			// redirection
            redirect('/admin/roles');
		 }
		// rendering view
		$this->_view('admin/add_role.php');	    
	}	
	
	/**
	* This method handle delete role process
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function delete($id) 
	{
        $this->user->delete_role($id = NULL);
        redirect('/admin/users', 'refresh');
    }
	
	/**
	* This method list form of all permissions and roles.
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function permissions()
	{
		$role_id = $this->permission->get_user_role(1);
		//$role_title = $this->permission->get_user_role_by_id($role_id);
		$user_permissions = $this->permission->get_user_permissions($role_id);				
		$data['user_permissions'] = $user_permissions;
		$data['permissions'] = $this->permission->get_permissions();
		$data['roles']  = $this->user->get_roles();
		$data['page']	=	$this->config->item( 'ci_cms_template_dir_admin' ) . "permissions";
		//$this->load->view( $this->_container, $data );
		$this->_view('admin/permissions.php', $data);	    
	}
	
	/**
	* This method handle ad permission form process.
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function addPermission()
	{	
		if ($this->input->post('name')) 
		{
			$data['name'] = $this->input->post('name');
            $data['permission_key'] = $this->input->post('permission_key'); 
			$this->role->add_permission($data);
            redirect('/admin/roles/permissions/');
		 } 
		$data['page'] = $this->config->item('ci_cms_template_dir_admin') . "add_permission";
		$this->_view('admin/add_permission.php', $data);	    
	}
	
	/**
	* This method handle ad permission form process.
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function updateCap()
	{
		$roles = $this->user->get_roles();	
		if ($this->input->post('capability')) 
		{
			$caps	=	$this->input->post('capability');
			$capabilities = $this->role->add_permissions_to_roles( $caps, $roles );   	
				
		 }
		 $data['permissions'] = $this->permission->get_permissions();
		 $data['roles']  = $roles;
		 $this->_view('admin/permissions.php', $data);	  
	}
	
	
}

/* End of file Roles.php */
/* Location: application/controllers/admin/Roles.php */