<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {
	 
	
	 function __construct() {
        parent::__construct();
     }
	
	/**
	* This method list all users
	* @access public
	* @params 
	* @return render view
	* @author AsifU
	*/
    public function index() {	
		$users = $this->user->get_all_users();	
		$data['users'] = $users;
		$data['roles'] = $this->user->role_dropdown();	
        $this->_view('admin/users_list.php',$data);		
    }
	
	/**
	* This method handle create user process.
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function create() {
		$this->load->library(array('ion_auth','form_validation'));
		$this->form_validation->set_rules( 'first_name', 'First Name', 'required' );
		$this->form_validation->set_rules( 'last_name', 'Last Name', 'required' );
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[tbl_users.user_name]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|matches[confirm_password]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');		
		
		if ($this->form_validation->run() == true){
			$fname = $this->input->post('first_name');
			$lname = $this->input->post('last_name');
			$email    = strtolower($this->input->post('email'));
            $username = $this->input->post('user_name');
            $password = $this->input->post('password');
			$password = $this->bcrypt->hash($password);
			$user_role = $this->input->post('user_role');
			$user_data = array(
							'user_name'	=> $username,
							'email'		=> $email,
							'user_name'	=> $username,
							'first_name'	=> $fname,
							'last_name'	=> $lname,
							'password'	=> $password,
						);
			$this->user->create_user( $user_data, $user_role );
		}
		
		$users = $this->user->get_all_users();	
		$data['roles'] = $this->user->role_dropdown();	
		$data['users'] = $users;		
		$this->_view('admin/users_list.php', $data);
		
	}
	
	/**
	* This method handle edit user process
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/
	public function edit( $user_id ) {
		if ($this->input->post('first_name')) {
            $data['first_name'] = $this->input->post('first_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['user_name'] = $this->input->post('user_name');
            $data['email'] 	   = $this->input->post('email');            
            $this->user->update($data, $user_id);
			$role['role_id'] = $this->input->post('role_id');
			$this->user->update_role( $role, $user_id );
            redirect('/admin/users', 'refresh');
        }
		
		$role_name = $this->user->get_user( $user_id );
		
		$user = $this->user->get_user( $user_id );
		$user_roles = $this->user->get_roles();
		$data['user'] = $user;
		$data['user_roles'] = $user_roles;
        $this->_view('admin/users_edit.php', $data);
	}
	
	public function delete( $id ) {
        $this->user->delete( $id );
        redirect('/admin/users', 'refresh');
    }
	
}
/* End of file Users.php */
/* Location: application/controllers/admin/Users.php */