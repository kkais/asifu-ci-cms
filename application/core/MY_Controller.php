<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    var $_container;
    var $_modules;

    function __construct() {
        parent::__construct();
		$this->load->library(array('permission' )); // custom permission library
		$this->load->helper('common'); // custom helper to have all common functions
    }
	
	/**
	* This method handle layout of app
	* @access public
	* @params view file name
	* @params array of data
	* @param html header to include js and css files in head tag.
	* @param show html, if now view file then simple render html.
	* $param page title
	* @return void()
	* @author AsifU
	*/
	function _view($view, $params = array(), $html_header = "", $showhtml = "", $page_title="CI CMS")
	{
		
		$path = 'templates/';
		
		if ( ! empty($view)) { // provided view will load
			$html = $this->load->view($view, $params, TRUE);
		}
		else { // only html will render
			$html = $showhtml;
		}
		
		 $params = array('page_html' => $html, // body content
		  'html_header'          => $html_header, // html head tag
		  'page_title'          => $page_title
		 );
		 
		$this->load->view($path . 'header.php', $params);
		$this->load->view($path . 'footer.php');
	}
}

class Admin_Controller extends CI_Controller {

    var $_container;
    var $_modules;
    function __construct() {
        parent::__construct();
        $this->load->config('ci_cms');
		$this->load->helper(array('url','language','form'));
		$this->load->library(array('ion_auth', 'permission' )); // custom auth library
		if ( !$this->permission->is_site_admin() ) redirect('/auth', 'refresh');	
		$this->_container = $this->config->item('ci_cms_template_dir_admin_templates') . "layout.php";
		$this->load->model(array('admin/user'));    
    }
	
	/**
	* This method handle layout of app
	* @access public
	* @params view file name
	* @params array of data
	* @param html header to include js and css files in head tag.
	* @param show html, if now view file then simple render html.
	* $param page title
	* @return void()
	* @author AsifU
	*/
	function _view($view, $params = array(), $html_header = "", $showhtml = "", $page_title="CI CMS")
	{
		
		$path = 'Admin/templates/';
		
		if ( ! empty($view)) { // provided view will load
			$html = $this->load->view($view, $params, TRUE);
		}
		else { // only html will render
			$html = $showhtml;
		}
		
		 $params = array('page_html' => $html, // body content
		  'html_header'          => $html_header, // html head tag
		  'page_title'          => $page_title
		 );
		 
		$this->load->view($path . 'header.php', $params);
		$this->load->view($path . 'footer.php');
	}
}