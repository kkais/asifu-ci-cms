<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter common helper to have all important functions used in whole website
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Common
 * @author		AsifU
 */
 
 	/**
	* This method if current page is home or other page
	* @access public
	* @return bool
	* @author AsifU
	*/
	function is_home() 
	{
		// initialize var
		$CI =& get_instance();
		$current_url = $CI->uri->uri_string();
		// if controller = home or empty return true
		if($current_url=='' || $current_url=='home') return TRUE;
		// else
		return FALSE;
	}	
	
	/**
	* This method retrieve all subcategories of parent category
	* @access public
	* @param  parent category id
	* @return array of subcategories
	* @author AsifU
	*/
	function get_sub_categories($parent=0){
		$CI =& get_instance();
		$query = $CI->db->where('cat_parent', $parent)->get('categories');
		return $query->result();		
	}
	
	/**
	* This 
	* @access public
	* @param  price int
	* @return formated price
	* @author AsifU
	*/
	function format_price($price){
		$CI =& get_instance();
		//setlocale(LC_MONETARY,"en_US");
		//$price = money_format('',$price);
		$currency = '$';
		$price = number_format($price, 2);
		$price = $currency.$price;
		return $price;
		
	}
	
	/**
	* This method track if user is logged in
	* @access public
	* @return true of false
	* @author AsifU
	*/
	function is_user_logged_in()
	{
		$CI =& get_instance();
		return (bool) $CI->session->userdata('logged_in');
	}
	
	/**
	* This method create categories and subcategories hierarchy used in shop page sidebar
	* @access public
	* @param int parent id
	* @return html tree of all categories for angular js 
	* @author AsifU
	*/
	function getTreeAngular($parent = 0) {
		$CI =& get_instance();		
		$query = $CI->db->where('cat_parent', $parent)->get('categories');
		$first_level = $query->result();        
        $tree = '<ul>';
        foreach ($first_level as $fl) {
			$query = $CI->db->where('cat_parent', $fl->id)->get('categories');
            $count = $query->num_rows();
            if ($count != 0) {
                $tree .= '<li><div class="checkbox"><input type="checkbox" name="subcats[]" ng-model="filterCatId['.$fl->id.']"><span>' . $fl->name . '</span></div>';
               $tree .= getTreeAngular($fl->id);
            } else {
                $tree .= '<li><div class="checkbox"><input type="checkbox" name="subcats[]" ng-model="filterCatId['.$fl->id.']">' . $fl->name . '</div></a>';
            }
            $tree .= '</li>';
        }
        $tree .= '</ul>';
        return $tree;
    }
	
	function getBrands() {
		$CI =& get_instance();		
		$query = $CI->db->get('brands');
		$results = $query->result(); 
		$html = '';
		foreach ($results as $result) { 
			$html .= '<div class="checkbox">';
			$html .= '<label>
                      <input type="checkbox" name="" value="'.$result->id.'" ng-model="pbrands['.$result->id.']">
					  '.$result->name.' 
                      </label>';
			$html .= '</div>';			
		}
		return $html;
	}