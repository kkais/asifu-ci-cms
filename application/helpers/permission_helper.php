<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Permission Helper
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Permission
 * @author		AsifU
 */

if ( ! function_exists('test_method'))
{
	/**
	* This method checked if permission exist for a specific role or not
	* @access public
	* @param permissionkey e.g edit_page
	* @param role id
	* @return bool
	* @author AsifU
	**/
    function permission_exist($role_id, $permission_key)
    {
		// initialize var
		$CI =& get_instance();
		// retrieve all permissions
		$user_permissions = $CI->permission->get_user_permissions($role_id);
		if(in_array( $permission_key, $user_permissions )) return TRUE;	
		// else
		return FALSE;        
    }
	
	/**
	* This method get user data by user id
	* @access public
	* @param user id
	* @return array of user data
	* @author AsifU
	**/
	function get_user_data_by_id($user_id)
    {
		// initialize var
		$CI =& get_instance();
		//query
		$CI->db->select('first_name, last_name');
		$CI->db->where('id', $user_id);
		$query = $CI->db->get('tbl_users');
		// get results
		$result = $query->row();       
		return $result;
    }   
}