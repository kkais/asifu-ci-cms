<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Slug Helper For products and categories
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Slug
 * @author		AsifU
 */
 
 	/**
	* This method create slug from product title by removing spaces etc
	* @access public
	* @params product title
	* @return seo friendly slug
	* @author AsifU
	*/
	function create_slug($string) 
	{
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}	