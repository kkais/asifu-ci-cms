<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Permission Class
* Description:
* The Permission class uses different functions of user permissions
**/
class Permission {
	
	// init vars
    var $CI;                        // CI instance
    var $where = array();
    var $set = array();
    var $required = array();
	
	function __construct() 
	{
		
		// init vars
        $this->CI =& get_instance();
		//$this->CI->load->library('session');
	}

	// get all permissions, for the purposes of listing them in a form
	public function get_permissions()
	{
		$query = $this->CI->db->get('permissions');
        return $query->result();
	}	
	/**
	* This method join threes table and get role id by user id
	* @access public
	* @return role id
	* @author AsifU
	*/
	public function get_user_role($user_id)
	{
		$this->CI->db->select('c.name as role_title, c.role_id as role_id');
		$this->CI->db->join('users_roles b', 'a.id = b.user_id', 'inner');
		$this->CI->db->join('roles c', 'b.role_id = c.role_id', 'inner');
		$this->CI->db->where('b.user_id', $user_id);
		$query = $this->CI->db->get('tbl_users a');
		$result = $query->row();
		if ($query->num_rows()){
			return $result->role_id;
		}		
	}
	
	/**
	* This method get role title by role id
	* @parm role id fo logged in user
	* @access public
	* @return role title
	* @author AsifU
	*/
	public function get_user_role_by_id($role_id)
	{
		$this->CI->db->select('name');
		$this->CI->db->where('role_id', $role_id);
		$query = $this->CI->db->get('roles');
		$result = $query->row();
		if ($query->num_rows())
		{
			return $result->name;
		}
	}
	
	/**
	* This method check for cetain permission : edit_page
	* @access public
	* @param permissionkey e.g edit_page
	* @return bool
	* @author AsifU
	*/
	public function user_has_permission($permission_key)
	{
		$user_id = $this->get_user_id();
		//if userid not null
		if($user_id){
			// get logged in user role id
			$role_id = $this->get_user_role($user_id);	
			// all permissions
			$user_permissions = $this->get_user_permissions($role_id);
			// check permission in array
			if(in_array( $permission_key, $permissions )) return TRUE;		
		}
		return FALSE;	
	}
	
	/**
	* This method return all permission of specific role
	* @access public
	* @param role id
	* @return array of permission keys
	* @author AsifU
	*/
	public function get_user_permissions($role_id)
	{
		if($role_id)
		{
			// query to select permission keys
			$this->CI->db->select('permission_id');
			$this->CI->db->where('role_id', $role_id);
			$query = $this->CI->db->get('users_permissions');
			$result = $query->row();
			return unserialize($result->permission_id);
		}
	}
	
	/**
	* This method track if user is logged in
	* @access public
	* @return true of false
	* @author AsifU
	*/
	public function is_user_logged_in()
	{
		return (bool) $this->CI->session->userdata('logged_in');
	}
	
	/**
	* This method check if user is admin
	* @access public
	* @return bool
	* @author AsifU
	*/
	public function is_site_admin()
	{
		$user_id = $this->get_user_id();
		if( $user_id )
		{
			$query = $this->CI->db->select('user_id')->where( array( 'user_id' => $user_id, 'role_id' => 1 ) )->get('users_roles');		
			if( $query->num_rows() > 0 ) return true;
		}
		return false;
	}
	
	/**
	 * logged_in
	 *
	 * @return integer
	 * @author jrmadsen67
	 **/
	public function get_user_id()
	{
		$user_id = $this->CI->session->userdata('user_id');
		if (!empty($user_id))
		{
			return $user_id;
		}
		return null;
	}
	// add permissions to a role, each permission must have an input name of "perm1", or "perm2" etc
	
}