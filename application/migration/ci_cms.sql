-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2016 at 08:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`) VALUES
(1, 'Armani', '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `cat_parent` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `cat_parent`) VALUES
(12, 'Men', 'men', '<p>test</p>', 0),
(13, 'Women', 'women', '', 0),
(26, 'Sandals', 'sandals', '', 13),
(27, 'Shirts', 'shirts', '', 13),
(28, 'Shoes', 'shoes', '', 12),
(29, 'Pc', 'pc', '', 12);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `img_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` longtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option_name`, `option_value`, `status`) VALUES
(23, 'payment_options', 'a:2:{i:0;s:15:"paypal_standard";i:1;s:23:"paypal_express_checkout";}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `order_note` longtext NOT NULL,
  `total_amount` int(11) NOT NULL,
  `payer_email` varchar(100) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `txn_id` varchar(200) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `company`, `address`, `phone`, `order_note`, `total_amount`, `payer_email`, `currency_code`, `payment_method`, `payment_status`, `txn_id`, `order_date`) VALUES
(10, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 143, '', '', 'paypal', 'on hold', '', '2016-06-21 18:04:28'),
(11, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 120, 'asif@webcomers.com', 'USD', 'paypal', 'Completed', '1DP58351SY7258740', '2016-06-21 15:24:35'),
(12, 1, 'Datumsquare', 'Karachi pakistan', '532453', 'Special Note', 143, '', '', 'paypal', 'on hold', '', '2016-06-21 18:06:51'),
(13, 1, 'Datumsquare', 'Karachi pakistan', '532453', 'Special Note', 143, 'asif@webcomers.com', 'USD', 'paypal', 'Completed', '8KN60654NH759424U', '2016-06-21 18:23:38'),
(14, 1, 'Datumsquare', 'Karachi pakistan', '532453', 'Special Note', 383, 'asif@webcomers.com', 'USD', 'paypal', 'completed', '9SE91930PC9761416', '2016-06-21 19:16:20'),
(15, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 120, '', '', 'paypal', 'on hold', '', '2016-06-21 20:31:31'),
(16, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 240, '', '', 'paypal', 'on hold', '', '2016-06-21 20:50:50'),
(17, 1, '', '', '', '', 433, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '4R958054UN942901N', '2016-06-22 18:30:13'),
(18, 1, '', '', '', '', 553, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1UM48870GX0430929', '2016-06-22 18:32:36'),
(19, 1, '', '', '', '', 553, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1UM48870GX0430929', '2016-06-22 18:40:52'),
(20, 1, '', '', '', '', 553, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1UM48870GX0430929', '2016-06-22 18:41:58'),
(21, 1, '', '', '', '', 553, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1UM48870GX0430929', '2016-06-22 18:42:13'),
(22, 1, '', '', '', '', 553, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1UM48870GX0430929', '2016-06-22 18:42:36'),
(23, 1, 'toyota', 'test', '34324234', 'Special Note', 145, '', '', 'paypal', 'on hold', '', '2016-06-23 13:37:17'),
(24, 1, '', '', '', '', 145, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '2GM42705BR920211Y', '2016-06-23 13:39:00'),
(25, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 120, '', '', 'paypal', 'on hold', '', '2016-06-23 19:14:05'),
(26, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 120, '', '', 'paypal', 'on hold', '', '2016-06-23 19:14:37'),
(27, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 120, '', '', 'paypal', 'on hold', '', '2016-06-23 19:14:45'),
(28, 1, 'Allshore', 'Islamabad pakistan', '532453', 'Special Note', 99, 'asif.u-buyer@allshoreresources.com', 'USD', 'paypal', 'completed', '1YH892126P853794J', '2016-06-24 14:59:10'),
(29, 1, '', '', '', '', 286, 'LJ48QNE3LLU8L', '', 'paypal', 'Completed', '1A574163PX214353S', '2016-06-24 15:02:39');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `name`, `qty`, `price`, `subtotal`, `product_id`, `order_id`) VALUES
(9, 'Fur coat', 2, 120, 240, 18, 7),
(10, 'Black Blouse Versace', 1, 143, 143, 19, 7),
(11, 'White Blouse Armani', 2, 145, 290, 17, 7),
(12, 'White Blouse Armani', 1, 145, 145, 17, 8),
(13, 'White Blouse Armani', 1, 145, 145, 17, 9),
(14, 'Black Blouse Versace', 1, 143, 143, 19, 10),
(15, 'Fur coat', 1, 120, 120, 18, 11),
(16, 'Black Blouse Versace', 1, 143, 143, 19, 12),
(17, 'Black Blouse Versace', 1, 143, 143, 19, 13),
(18, 'Fur coat', 2, 120, 240, 18, 14),
(19, 'Black Blouse Versace', 1, 143, 143, 19, 14),
(20, 'Fur coat', 1, 120, 120, 18, 15),
(21, 'Fur coat', 2, 120, 240, 18, 16),
(22, 'White Blouse Armani', 2, 145, 290, 17, 17),
(23, 'Black Blouse Versace', 1, 143, 143, 19, 17),
(24, 'White Blouse Armani', 2, 145, 290, 17, 18),
(25, 'Black Blouse Versace', 1, 143, 143, 19, 18),
(26, 'Fur coat', 1, 120, 120, 18, 18),
(27, 'White Blouse Armani', 2, 145, 290, 17, 19),
(28, 'Black Blouse Versace', 1, 143, 143, 19, 19),
(29, 'Fur coat', 1, 120, 120, 18, 19),
(30, 'White Blouse Armani', 2, 145, 290, 17, 20),
(31, 'Black Blouse Versace', 1, 143, 143, 19, 20),
(32, 'Fur coat', 1, 120, 120, 18, 20),
(33, 'White Blouse Armani', 2, 145, 290, 17, 21),
(34, 'Black Blouse Versace', 1, 143, 143, 19, 21),
(35, 'Fur coat', 1, 120, 120, 18, 21),
(36, 'White Blouse Armani', 2, 145, 290, 17, 22),
(37, 'Black Blouse Versace', 1, 143, 143, 19, 22),
(38, 'Fur coat', 1, 120, 120, 18, 22),
(39, 'White Blouse Armani', 1, 145, 145, 17, 23),
(40, 'White Blouse Armani', 1, 145, 145, 17, 24),
(41, 'Fur coat', 1, 120, 120, 18, 25),
(42, 'Fur coat', 1, 120, 120, 18, 26),
(43, 'Fur coat', 1, 120, 120, 18, 27),
(44, 'White Blouse Versace', 1, 99, 99, 20, 28),
(45, 'Black Blouse Versace', 2, 143, 286, 19, 29);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_title` varchar(200) NOT NULL,
  `page_content` longtext NOT NULL,
  `page_status` varchar(100) NOT NULL DEFAULT 'publish',
  `page_slug` varchar(100) NOT NULL,
  `page_author` int(11) NOT NULL,
  `page_created` datetime NOT NULL,
  `page_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `page_type` varchar(100) NOT NULL DEFAULT 'page',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_title`, `page_content`, `page_status`, `page_slug`, `page_author`, `page_created`, `page_modified`, `page_type`, `status`) VALUES
(1, 'About', '<p><strong>Text goes here</strong></p>', 'publish', 'about', 1, '2016-06-07 22:24:35', '2016-06-07 20:24:35', 'page', 0),
(3, 'Services', '<p>services description</p>', 'publish', 'services', 1, '2016-06-08 17:03:29', '2016-06-08 15:03:29', 'page', 0),
(4, 'Test', '<p>Test description</p>', 'publish', 'test', 1, '2016-06-08 17:26:32', '2016-06-08 15:26:32', 'page', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `permission_key` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `permission_key`, `category`) VALUES
(1, 'Edit Page', 'edit_page', ''),
(2, 'Delete Page', 'delete_page', ''),
(3, 'Delete other page', 'delete_other_page', ''),
(4, 'Edit post', 'edit_post', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` longtext NOT NULL,
  `price` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `image_id` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `type` varchar(200) NOT NULL DEFAULT 'simple',
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `image`, `image_id`, `slug`, `brand_id`, `created_at`, `type`, `status`) VALUES
(17, 'White Blouse Armani', '<p>White lace top, woven, has a round neck, short sleeves, has knitted lining attached.</p>', '145', 'product2.jpg', '', 'white-blouse-armani', 0, '2016-06-14 19:57:36', 'simple', 0),
(18, 'Fur coat', '', '120', 'product1_2.jpg', '', 'fur-coat', 0, '2016-06-14 19:58:35', 'simple', 0),
(19, 'Black Blouse Versace', '', '143', 'product3.jpg', '', 'black-blouse-versace-1', 0, '2016-06-14 19:59:13', 'simple', 0),
(20, 'White Blouse Versace', '<p>In our Ladies department we offer wide selection of the best products we have found and carefully selected worldwide.</p>', '99', 'product2.jpg', '', 'white-blouse-versace', 0, '2016-06-14 20:00:07', 'simple', 0),
(21, 'Test', '<p>It has become very easy to get access to relevant information at any time anywhere.&nbsp;</p>', '36', 'blog6.jpg', '', 'test', 0, '2016-06-15 17:18:17', 'simple', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`id`, `product_id`, `category_id`) VALUES
(15, 17, 12),
(16, 18, 27),
(17, 19, 13),
(18, 20, 12),
(19, 21, 28);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `description`) VALUES
(1, 'Admin', ''),
(2, 'Subscriber', ''),
(3, 'Author', ''),
(4, 'Editor', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activation_code` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `email`, `first_name`, `last_name`, `password`, `activation_code`, `last_login`, `ip_address`, `created_on`) VALUES
(1, 'admin', 'asif@gmail.com', 'Asif', 'ullah', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', '0000-00-00 00:00:00', '', '2016-06-03 19:41:06'),
(5, 'asif12345', 'asif.u@allshorerescources.com', 'Ahmed', 'ali', '$2y$08$bh0/WawU.PjKNcakk.np8egvrEx.Qq6K3UUH9WEA7FvHNUAtzBFzS', '', '0000-00-00 00:00:00', '', '2016-06-03 17:12:18'),
(10, 'admin123', 'asiffdsa@gmail.com', 'asif', 'khannn', '$2y$08$YDZQtP5m9U.GNbL4uFvXouLqAS1yvpJ.SnKfdxmvkVBlj08Pa59O.', '', '0000-00-00 00:00:00', '', '2016-06-08 20:04:39'),
(11, 'test123', 'asif.u123@allshorerescources.com', 'Test', 'Test', '$2y$08$7u51sMSQRVv5T7U8RFpff./DfJ4KCeRmOtwO7GwVX81XU0mtwGda6', '', '0000-00-00 00:00:00', '', '2016-06-08 19:42:58'),
(12, 'safdsa', 'asif34.u@allshorerescources.com', 'asif', 'ullah', '$2y$08$.uUn1QPJStd7myZTNI4qfuaPDK4Y7wNLPXMgT4B/wKFZVpZVX6UUi', '', '0000-00-00 00:00:00', '', '2016-06-08 20:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `permission_id` varchar(200) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `permission_id`, `role_id`) VALUES
(52, 'a:4:{i:1;s:9:"edit_page";i:2;s:11:"delete_page";i:3;s:17:"delete_other_page";i:4;s:9:"edit_post";}', 1),
(53, 'a:1:{i:1;s:9:"edit_page";}', 2),
(54, 'a:2:{i:1;s:9:"edit_page";i:2;s:11:"delete_page";}', 3),
(55, 'a:3:{i:1;s:9:"edit_page";i:2;s:11:"delete_page";i:3;s:17:"delete_other_page";}', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`) VALUES
(2, 1, 1),
(3, 5, 2),
(8, 10, 3),
(9, 11, 2),
(10, 12, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
