<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends CI_Model{
	
	public function add_permission($data) {
        $success = $this->db->insert('permissions', $data);
        if ($success) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
	
	public function add_permissions_to_roles( $caps, $roles ){
		
		foreach ( $roles as $role )
		{
			if( array_key_exists( $role->name, $caps ) )
			{
				$this->save_role_permissions( $caps[$role->name], $role->role_id ); // permissions and role id
			}
		}	
	}
	public function save_role_permissions( $caps_data, $role_id ){
		// delete all permissions on this role id first
        $this->db->where('role_id', $role_id);
        $this->db->delete('users_permissions');
		
		// Insert permissions	
		$this->db->set('role_id', $role_id);
		$this->db->set('permission_id', serialize($caps_data));
		$this->db->insert('users_permissions');
		
	}
}