<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model{
	
	public function get_all_users(){
		$query = $this->db
				->select('a.id,a.user_name,a.first_name,a.last_name,a.email,a.created_on, b.user_id, b.role_id, c.name as role_title')
				->join('users_roles b', 'a.id = b.user_id', 'inner')
				->join('roles c', 'b.role_id = c.role_id', 'inner')
				->get('tbl_users a');
        return $query->result();
	}
	
	public function get_user( $user_id ){
		$query = $this->db->select('a.id,a.user_name,a.first_name,a.last_name,a.email, b.user_id, b.role_id')->join('users_roles b', 'a.id = b.user_id', 'inner')->where('a.id', $user_id)->get('tbl_users a');
        return $query->row();
	}
	
	public function get_roles(){
		$query = $this->db->get('roles');
        return $query->result();
	}
		
	public function update($data, $id) {
        $query = $this->db->where('id', $id)->update('tbl_users', $data);
        return $query;
    }
	
	public function update_role($data, $user_id) {
        $query = $this->db->where('user_id', $user_id)->update('users_roles', $data);
        return $query;
    }
	
	public function delete($user_id) {
		$query = "DELETE a, b FROM tbl_users a
				  INNER JOIN users_roles b ON a.id = b.user_id
				  WHERE a.id = ".$user_id."";
		$res	= $this->db->query( $query );
		return $res;		
        
	}
	
	public function add_role($data) {
        $success = $this->db->insert('roles', $data);
        if ($success) {
            return $this->db->insert_id();
        }else {
            return FALSE;
        }
    }
	
	public function delete_role($id) {
       return $this->db->where('role_id', $id)->delete('roles');
    }
	
	public function total_users(){
		$query = $this->db->select('*')->get('tbl_users');		                  						  
		return $query->num_rows();
	}
	
	/**
	 * register
	 *
	 * @return bool
	 * @author Asif.u
	 **/
	public function create_user( $user_data, $user_role ){
		
		if( !empty( $user_data ) ){
			$this->db->insert( 'tbl_users', $user_data );
			$id = $this->db->insert_id();
		}
		if( $id ){
			$this->add_user_role( $user_role, $id );
		}
		
		return (isset($id)) ? $id : FALSE;
	}
	
	/**
	 * add_user_role
	 *
	 * @return bool
	 * @author Asif.u
	 **/
	public function add_user_role( $role, $user_id ){
		if( empty( $role ) ) $role = 2;
		$role_data = array( 'role_id' => $role, 'user_id' => $user_id );
		$this->db->insert( 'users_roles', $role_data );
	}
	
	/**
	 * get roles dropdown options
	 *
	 * @return array of options
	 * @author Asif.u
	 **/
	public function role_dropdown(){
		$query  = $this->db->get('roles');		
        $results = $query->result();
		$options = array();
		foreach( $results as $key => $result ){
			$options[] = $result->name;
		}
		return $options;
	}
}