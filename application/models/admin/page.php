<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Model{
	
	
	/**
	* This method retrieve all pages data
	* @access public
	* @params 
	* @return array of page data
	* @author AsifU
	*/
	public function get_pages()
	{
		$query = $this->db->get('pages');
        return $query->result();
	}
	
	/**
	* This method add page
	* @access public
	* @params array of page data
	* @return page id
	* @author AsifU
	*/
	public function add_page($data) 
	{
		//$data['']
        $success = $this->db->insert('pages', $data);
        if ($success) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
	/**
	* This method get page by page id
	* @access public
	* @params page id
	* @return page data in array
	* @author AsifU
	*/
	public function get_page( $page_id )
	{
		$this->db->select('id, page_title, page_content');
		$this->db->where('id', $page_id);
		$query = $this->db->get('pages');
		$result = $query->row();
		if ($query->num_rows())
		{
			return $result;
		}
	}
	
	/**
	* This method update specific page
	* @access public
	* @params page title
	* @return seo friendly slug
	* @author AsifU
	*/
	public function update($data, $id) {
        $query = $this->db->where('id', $id)->update('pages', $data);
        return $query;
    }	
	
	public function delete($page_id) {
		$this->db->where('id', $page_id);
		$this->db->delete('pages');         
	}
	/**
	* This method convert page title to seo friendly slug
	* @access public
	* @params page title
	* @return seo friendly slug
	* @author AsifU
	*/
	public function page_url($string) 
	{
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return $string;
	}
	
	
	
}