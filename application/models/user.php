<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model
{
	/**
	 * register
	 *
	 * @return bool
	 * @author Asif.u
	 **/
	public function register_process( $user_data, $user_role ){
		
		if( !empty( $user_data ) ){
			$this->db->insert( 'tbl_users', $user_data );
			$id = $this->db->insert_id();
		}
		if( $id ){
			$this->add_user_role( $user_role, $id );
		}
		
		return (isset($id)) ? $id : FALSE;
	}
	
	/**
	 * This funtion add role to a specific user.
	 * @return bool
	 * @author Asif.u
	 **/
	public function add_user_role( $role, $user_id ){
		if( empty( $role ) ) $role = 2;
		$role_data = array( 'role_id' => $role, 'user_id' => $user_id );
		$this->db->insert( 'users_roles', $role_data );
	}
	
	public function total_users(){
		$query = $this->db->select('*')->get('tbl_users');		                  						  
		return $query->num_rows();
	}
	
	/**
	* This method validate email address
	* @access public
	* @return true if success otherwise error message.
	* @author AsifU
	*/
	public function email_exists(){
		 // data array of http request		
		 $postdata = file_get_contents("php://input");
		 $request = json_decode($postdata);
		 
    	 // Assign posted emailId to $email
		 $email =  $request->user_email; 
		 
		 // Select where email matches the posted user email
		 $query = $this->db->where('email', $email)->get('tbl_users');
		 
		 // Check if a row has matched our EmailId
		 if($query->num_rows() > 0) return TRUE;
		 
		 // if no email matched
		 return FALSE;
	}
	
}
