<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter cart class for products and categories
 *
 * Display of shop page and categories templates
 * @author		Asifu
 */
class Cart extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('cart_model');
	}
	
	/**
	* This method load cart view page
	* @access public
	* @return void()
	* @author AsifU
	*/
	function index()
	{
 		$cart_items = $this->cart->contents();
		$data['cart_items'] = json_encode($cart_items);
		$data['cart_total_items'] = $this->cart->total_items();
		$data['cart_total']		 = $this->cart->format_number($this->cart->total());
		//print_r()
		$this->_view('cart', $data);     
		
	}
	
	/**
	* This method to add items to the cart and return reposnse to ajax
	* @access public
	* @return response to ajax
	* @author AsifU
	*/
	function add_cart_item()
	{
  		//echo "success";				
		if($this->cart_model->validate_add_cart_item() == TRUE)
		{
			// cart items
			$data['cart_items'] = $this->cart->total_items();
			$data['success']	= 'success';
			$data['success_msg'] = 'New product has been added to your cart.';
			echo json_encode($data);
		}else{
			echo 'Something went wrong, Please try again';
		}    
	}
	
	/**
	* This method update your cart items
	* @access public
	* @return json encode form data of cart items
	* @author AsifU
	*/
	function update_cart()
	{
    	$data = $this->cart_model->validate_update_cart();
		$data = $this->get_cart_content();	
		echo json_encode($data);
		exit;
    	//redirect('cart/view');
	}
	
	/**
	* This method remove all items from your cart
	* @access public
	* @return void()
	* @author AsifU
	*/
	public function empty_cart()
	{
    	$this->cart->destroy(); // Destroy all cart data
		$data = $this->get_cart_content();		
		echo json_encode($data);
		exit;
    	//redirect('cart/view'); // Refresh te page
	}
	
	/**
	* This method delete cart single items (by ajax angular js)
	* @access public
	* @return json encode form data
	* @author AsifU
	*/
	public function delete_cart_item()
	{
		// data array of http request		
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$rowid	 = $request->rowid;
		$data = array(
			'rowid'   => $rowid,
			'qty'     => 0
		);
		$this->cart->update($data); 
		
    	$data = $this->get_cart_content();		
		echo json_encode($data);
		exit;
	}	
	
	
	/**
	* This method get all cart items and cart total
	* @access public
	* @return cart item data and total amount.
	* @author AsifU
	*/
	public function get_cart_content(){
		// cart items
		$data['cart_items'] = $this->cart->contents();
		// total amount of items
		$data['cart_total'] = $this->cart->format_number($this->cart->total());
		
		return $data;
	}
	
}
/* End of file cart.php */
/* Location: application/modules/cart/controllers/cart.php */