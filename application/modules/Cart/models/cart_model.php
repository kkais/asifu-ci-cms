<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter CART Model Class
 *
 * Contain all methods shop related
 * @author		Asifu
 */
class Cart_Model extends CI_Model{
	
	/**
	* This method handle validation of cart posted items
	* @access public
	* @return bool
	* @author AsifU
	*/
	public function validate_add_cart_item()
	{
		 // data array of http request		
		 $postdata = file_get_contents("php://input");
		 $request = json_decode($postdata);
		 
    	 // Validate posted data, and then add the item!		 
		 $id =  $request->product_id; // Assign posted product_id to $id
    	 $qty = $request->quantity; // Assign posted quantity to $cty	
		 
		 // Select where id matches the posted product id
		 $query = $this->db->where('id', $id)->get('products');
		 
    	// Check if a row has matched our product id
		if($query->num_rows() > 0)
		{	 
		  // We have a match!
		  foreach ($query->result() as $row)
          {
            // Create an array with product information
            $data = array(
                'id'      => $id,
                'qty'     => $qty,
                'price'   => $row->price,
                'name'    => $row->name
            );
            // Add the data to the cart using the insert function that is available because we loaded the cart library
            $this->cart->insert($data); 
             
            return TRUE; // Finally return TRUE
          }
		  
		}else{
			// Nothing found! Return FALSE! 
			return FALSE;
		}

	}
	
	/**
	* This method handle validation of updated cart / Ajax call
	* @access public
	* @return bool
	* @author AsifU
	*/
	public function validate_update_cart()
	{
		// data array of http request		
		 $postdata = file_get_contents("php://input");
		 $request = json_decode($postdata);
		 
    	// Get the total number of items in cart
		//$total = $request->total_items;		 
		// Retrieve the posted other information (array)
		$item =  $request->rowid;
		$qty =   $request->qty;
	 	//return $item[$i]
		// loop true all items and update them
		for($i=0;$i < count($item); $i++)
		{
			// Create an array with the products rowid's and quantities. 
				$data = array(
				   'rowid' => $item[$i],
				   'qty'   => $qty[$i]
				);			 
						
			// Update the cart with the new information
			$this->cart->update($data);
		}
		
		//return $data;

	}
}
/* End of file cart_model.php */
/* Location: application/modules/cart/model/cart_model.php */