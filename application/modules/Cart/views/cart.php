<!-- cart main container -->
<div class="container" id="cartlist" ng-controller="cartListCtrl" ng-init="total_items='<?php echo $cart_total_items; ?>'; cartTotal='<?php echo $cart_total; ?>'; base_url='<?php echo base_url(); ?>'">

	<div id="basket" class="col-md-9">
		<div class="box">
			<!-- cart items -->
			<form method="post" ng-submit="updateCart()">						
				<h1>Shopping cart</h1>
				<p class="text-muted">You currently have {{total_items}} item(s) in your cart.</p>
				<div class="table-responsive">
					<table class="table" ng-if="total_items > 0">
						<thead>
							<tr>
								<th>Product</th>
								<th>Quantity</th>
								<th>Unit price</th>
								<th>Total</th>
								<th colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
							
							<!-- cart items loop -->
							<tr ng-repeat="item in cart_items">
								<input type="hidden" ng-model="item.rowid" name="rowid[]" value="{{item.rowid}}">
								<td><a href="#">{{item.name}}</a>
								</td>
								<td>
									<input type="number" ng-model="item.qty" min="0" name="qty[]" class="form-control" value="{{item.qty}}" ng-keyup="updateCart()" ng-mouseup="updateCart()">
								</td>
								<td>{{item.price | currency }}</td>
								<td>{{item.subtotal | currency}}</td>
								<td><a href="#" ng-click="deleteItem(item.rowid)"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<!-- # cart items loop -->
						</tbody>
						
						<!-- cart total amount -->
						<tfoot>
							<tr>
								<th colspan="5">Total</th>
								<th colspan="2">{{cartTotal | currency}}</th>
							</tr>
						</tfoot>
						<!-- # cart total amount -->
					</table>
					<!-- if no items in the cart show message -->
					<p ng-if="cart_items.length == 0">You don't have any items in the cart.</p>
					<!-- # if no items in the cart show message -->

				</div><!-- EOF table responsive -->	
						
				<!-- UPDATE CART and CHECKOUT button -->
				<div class="box-footer">
					<div class="pull-left">
						<a class="btn btn-default" href="{{base_url}}shop"><i class="fa fa-chevron-left"></i> Continue shopping</a>
					</div>
					<div class="pull-right">
						<a href="#" ng-click="emptyCart()" class="btn btn-default">Empty Cart</a>
						<button class="btn btn-default" type="submit"><i class="fa fa-refresh"></i> Update Cart</button>						
						<a class="btn btn-default" href="{{base_url}}checkout">Proceed to checkout <i class="fa fa-chevron-right"></i></a>
					</div>					
				</div>
				<!-- # cart total amount -->
			</form>						
			<!-- # cart items -->
         </div>
	</div>

</div><!-- # cart main container -->
<script>
// controller for cart items, main angular module app defined in header.
app.controller("cartListCtrl", function($scope, $http, $location){
	
	console.log($location);
	// Assign json form cart items
	$scope.cart_items = <?php echo $cart_items; ?>;
	
	// This function trigger when update cart button pressed(form submit) or quantity updated through keyboard in input field.
	$scope.updateCart = function () {		
		$scope.cartqty = []; // initialize items quantity array
		$scope.rowid   = []; // initialize unique id array (rowid)
		
		// push cart qty of all items  to a single array
		$.each($scope.cart_items, function(index, val){
			$scope.cartqty.push(val.qty);
		});
		
		// push cart row ids of all items to a single array
		$.each($scope.cart_items, function(index, val){
			$scope.rowid.push(val.rowid);
		});
		
		// Find total number of items through quantity
		$scope.total_items = 0;
		$.each($scope.cartqty,function() {
			$scope.total_items += this;
		});
		
		// Ajax http post call to save cart items in session.
		$http({
			method : "POST",
			url : $scope.base_url+'cart/update_cart',
			data : 
				{ 
					qty 		: $scope.cartqty,
					rowid		: $scope.rowid,
					total_items : $scope.total_items
				},
		}).then(function mySucces(response) {
			// handle response data
			$scope.cart_items = response.data.cart_items; // update all cart items 
			$scope.cartTotal = response.data.cart_total;  // update cart total	
		}, function myError(response) {
			// handle error show warning			
			console.log('error');
		});
		
		return false; // stop page refresh		
	}
	
	/**This function triggered when delete icon clicked 
	 * @param = unique if of item
	 * @return updated cartitems and carttotal
	**/
	$scope.deleteItem = function (rowid){		
		// check if user has confirm to delete cart item
		if(confirm('Are you sure you want to delete this item?')){
			// Ajax http post call to delete cart single item
			$http({
				method : "POST",
				url : $scope.base_url+'cart/delete_cart_item',
				data : 
					{ 
						rowid		:rowid // item unique id
					},
			}).then(function mySucces(response) {
				// handle response data
				$scope.cart_items = response.data.cart_items;
				$scope.cartTotal = response.data.cart_total;	
			}, function myError(response) {
				// handle error show warning			
				console.log('error');
			});
		}		
	}	
	
	// This function triggered when empty cart button clicked
	$scope.emptyCart = function (){
		$http({
			method : "POST",
			url : $scope.base_url+'cart/empty_cart',			
		}).then(function mySucces(response) {
			// handle success message
			$scope.cart_items = response.data.cart_items;
			$scope.cartTotal = response.data.cart_total;
			$scope.total_items = 0;	
		}, function myError(response) {
			// handle error show warning			
			alert('error');			
		});
	}
});

</script>