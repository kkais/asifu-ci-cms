<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Category controller Class
 *
 * Enable admin to manage categories
 * @author		Asifu
 */
class Category extends Admin_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('products/product_model');
		$this->load->model('category_model');
		$this->load->helper(array('form', 'slug', 'common'));
		$this->load->library(array('upload', 'form_validation'));
	}
	
	/**
	* This method list all categories in admin
	* @access public
	* @return void()
	* @author AsifU
	*/	
	public function index()
	{		
		$categories = $this->category_model->get_all_categories(); // get all categories		
		// save data in array		
		$data['categories'] = $categories;
		// load view
		$this->_view('admin/categories_list.php', $data);
	}
	
	/**
	* This method create category
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function add(){
		if($this->input->post('add_category'))
		{
			$error = FALSE;
			//validate input field
			$this->form_validation->set_rules( 'name', 'Name', 'required' );
			//input data
			$data['name'] = $this->input->post('name');
			$parent_id	  = $this->input->post('cat_parent');
			$data['cat_parent'] = $parent_id;
            $data['description'] = $this->input->post('description');		
			// product slug
			$slug	= create_slug($data['name']);			
			// check if slug exist
			if($this->category_model->category_slug_exist($slug, $parent_id))
			{
				// save error message temporarily			
				$this->session->set_flashdata('message', "Category with the name provided already exist.");
				$error = TRUE;
			}
			$data['slug'] 	= $slug;	
			//if no errors process form			
			if ($this->form_validation->run() === TRUE)
			{
				if(!$error){
					$id = $this->category_model->insert($data);
					redirect('admin/categories');				
				}
			}
		}
		$categories	  = $this->category_model->get_all_categories(); // get all categories for dropdown
		$data['categories'] = $categories; // save data in array		
		// load view
		$this->_view('admin/add_category.php', $data);
	}
	
	/**
	* This method delete category
	* @access public
	* @params category id
	* @return void()
	* @author AsifU
	*/	
	public function delete($id) 
	{	
        $this->category_model->delete($id);
        redirect('/admin/categories/', 'refresh');
    }
	
	/**
	* This method handle edit category process
	* @access public
	* @params $category_id int
	* @return void()
	* @author AsifU
	*/
	public function edit($category_id=NULL) 
	{
		if ($this->input->post('edit_category')) 
		{
			// validate input field
            $this->form_validation->set_rules( 'name', 'Name', 'required' );
			//input data
			$data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');			
			
			if ($this->form_validation->run() === TRUE)
			{
					        
				$this->category_model->update($data, $category_id);				
				redirect('/admin/categories', 'refresh');
				
			}
        }
		$category	  = $this->category_model->get_category($category_id); // 
		$data['category'] = $category;
        $this->_view('admin/edit_category', $data);
	}
	
}
/* End of file category.php */
/* Location: application/modules/category/controllers/category.php */