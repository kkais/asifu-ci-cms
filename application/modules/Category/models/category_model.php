<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter category model Class
 *
 * Enable admin to manage categories
 * @author		Asifu
 */
class Category_Model extends CI_Model{
	
	var $iteration_number = 0;
	
	/**
	* This method Retrieve All categories
	* @access public
	* @params parent id
	* @return array with all categories
	* @author AsifU
	*/
	public function get_all_categories($parent=0)
	{
		$query = $this->db->where('cat_parent', 0)->get('categories');
		return $query->result();
	}
		
	/**
	 * This funtion insert category
	 * @access public
	 * @params category data array
	 * @return bool
	 * @author Asif.u
	 **/
	public function insert($category_data)
	{
		
		if( !empty( $category_data ) )
		{
			$this->db->insert( 'categories', $category_data );
			$id = $this->db->insert_id();
		}
		return (isset($id)) ? $id : FALSE;
	}
	
	
	/**
	 * This method delete category
	 * @access public
	 * @params category id
	 * @return bool
	 * @author Asif.u
	 **/
	public function delete($id) 
	{		
		$this->db->where('id', $id);
		$this->db->delete('categories');          
	}
	
	/**
	 * This method retrive category datat by category id
	 * @access public
	 * @params category id
	 * @return array of category data
	 * @author Asif.u
	 **/
	public function get_category($id) 
	{		
		$query = $this->db->where('id', $id)->get('categories');
		return $query->row();
	}
	
	/**
	 * This method update category by category id
	 * @access public
	 * @params product data array
	 * @params product id
	 * @return bool
	 * @author Asif.u
	 **/
	public function update($data, $id) {
        $query = $this->db->where('id', $id)->update('categories', $data);
        return $query;
    }
	
	/**
	* This method checked for the slug in database
	* @access public
	* @params slug
	* @params parent id
	* @return bool
	* @author AsifU
	*/
	function category_slug_exist($slug, $parent_id=0) 
	{
		if($parent_id>0)
		{
			$this->db->where('cat_parent', $parent_id);
		}
		// query to retrieve slug in current parent category
		$query = $this->db->where(array('slug' => $slug))->get('categories');
		return $query->num_rows();

		
	}

	/*public function create_subcategory_slug($cat_id, $slug){
		$query = $this->db->where(array('slug' => $slug, 'cat_parent!=', $cat_id))->get('categories');
		$res   = $query->row();
		if(count($res)>0){
			$slug = $slug."-".$res->slug;
		}
		return $slug;
	}*/
}
/* End of file category_model.php */
/* Location: application/modules/products/model/category_model.php */