<script src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Categories
                <a  href="<?= base_url('admin/categories') ?>" class="btn btn-warning">Go back to categories</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
			<?php if(validation_errors() != FALSE || $this->session->flashdata('message') == TRUE ) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo validation_errors(); ?>
					<?php if ($this->session->flashdata('message')): ?>
						<?= $this->session->flashdata('message') ?>
					<?php endif; ?>
				</div>
			<?php } ?>
			
				
			
			<!----EOF image upload errors-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add New Category
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form method="POST" action="<?= base_url('admin/category/add/') ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" placeholder="Name" name="name">
                                </div>
									
								<?php if(count($categories)>0):?>
								<select name="cat_parent" class="form-control">	
									<option value="0">None</option>		
											
									<?php foreach($categories as $category):?>
										<?php $subcategories = get_sub_categories($category->id); ?>
										<?php 
											$class = ($category->cat_parent>0)?'level-1' : 'level-0'; 
											$prefix = ($category->cat_parent>0)?'-' : ''; 
										?>
										<option class="<?php echo $class; ?>" value="<?php echo $category->id; ?>"><?php echo $prefix.$category->name; ?></option>
										<?php foreach ($subcategories as $list1): ?>
											<option value="<?php echo $list1->id; ?>">-<?php echo $list1->name; ?></option>
										<?php endforeach; ?>
										
										
										
									<?php endforeach; ?>
								</select>
								<?php endif; ?>
								
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
								
								
								
								<input type="hidden" name="add_category" value="1">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>