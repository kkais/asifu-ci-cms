<div id="Modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
			<div class="modal-body" style="background-color: #fff;">
				<div class="panel-heading">
                    <h3 class="panel-title">Please Register</h3>
                </div>
				<div class="panel-body">
					<form method="POST" action="<?= base_url('admin/category/add/') ?>" enctype="multipart/form-data">
						<div class="form-group">
							<label>Title</label>
							<input class="form-control" placeholder="Name" name="name">
						</div>
						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control" name="description"></textarea>
						</div>
						<input type="hidden" name="add_category" value="1">
						<button type="submit" class="btn btn-primary">Save Changes</button>
                    </form>
				</div>
			</div>
		
	</div>
</div>