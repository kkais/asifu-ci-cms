<!-- PROCESS FORM WITH AJAX (NEW) -->

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Categories
                    <a  href="<?php echo base_url('admin/category/add')?>" class="btn btn-success">Add a new</a>
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
		
		
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
					
            <div class="panel panel-default">
                <div class="panel-heading">
                    Categories
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
									<th>Description</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($categories)): ?>
								
                                    <?php foreach ($categories as $key => $list): ?>
									
                                        <tr class="odd gradeX">
                                            <td><?php echo $list->id; ?></td>
                                            <td><?php echo $list->name; ?></td>
                                            <td><?php echo $list->description; ?></td>
                                            <td>
                                                
                                                <a href="<?php echo base_url('admin/category/delete/'.$list->id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this product?');">Delete</a>
												 <a href="<?php echo base_url('admin/category/edit/'.$list->id); ?>" class="btn btn-warning">Edit</a>
                                            </td>
                                        </tr>
										
										<?php $subcategories = get_sub_categories($list->id); ?>
										
										<?php if (count($subcategories)): ?>
											<?php foreach ($subcategories as $key => $list1): ?>
												<tr class="odd gradeX">
													<td><?php echo $list1->id; ?></td>
													<td>—<?php echo $list1->name; ?></td>
													<td><?php echo $list1->description; ?></td>
													<td>
														<a href="<?php echo base_url('admin/category/delete/'.$list1->id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this product?');">Delete</a>
														 <a href="<?php echo base_url('admin/category/edit/'.$list1->id); ?>" class="btn btn-warning">Edit</a>
													</td>
												</tr>
											<?php endforeach; ?>
										<?php endif; ?>
										
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="even gradeC">
                                        <td>No data</td>
                                        <td>No data</td>                                        
                                        <td>No data</td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                            <tfooter>
                                <tr>
                                  	<th>ID</th>
                                    <th>Name</th>
									<th>Description</th>
									<th>Action</th>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>
<!-- /#page-wrapper -->
