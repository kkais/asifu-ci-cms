<script src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Products
                <a  href="<?= base_url('admin/products') ?>" class="btn btn-warning">Go back to products listing</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
			<?php if(validation_errors() != FALSE) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo validation_errors(); ?>
					<!--- validation used for image upload-->
				</div>
			<?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Category
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
							<form method="POST" action="<?= base_url('admin/category/edit/'. $category->id) ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" placeholder="Name" value="<?= $category->name ?>" name="name">
                                </div>
								
								
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"><?= $category->description ?></textarea>
                                </div>
								
								<input type="hidden" name="edit_category" value="1">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </form>
							
                            
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>