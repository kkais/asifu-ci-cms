<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter checkout class to handle checkout process and orders
 *
 * Display of checkout forms
 * @author		Asifu
 */
class Checkout extends MY_Controller {
	
	
	var $user_id = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('cart/cart_model');
		$this->load->model('checkout_model');
		$this->load->model('settings/setting_model'); // model for settings
		// load paypal custom library
		$this->load->library(array('paypal_lib', 'bcrypt'));
		// Load PayPal library
		$this->config->load('paypal');
		
		$this->user_id = $this->permission->get_user_id();
	}
	
	/**
	* This method load checkout form and check for login users
	* @access public
	* @return void()
	* @author AsifU
	*/
	function index()
	{		
		// restrict user from checkout if cart is empty
		if($this->cart->total()==0)
			redirect('cart');
		
		$user_logged_in			 = ($this->permission->is_user_logged_in())?FALSE:TRUE;
		$data['logged_in']		 = $user_logged_in;
		if($this->permission->is_user_logged_in())
		{			
			$user = $this->checkout_model->get_checkout_user_data($this->user_id);
			$data['user'] = $user;
		}else{
			$data['user'] = '';
		}
		$data['payment_options'] = $this->setting_model->get_option('payment_options');
 		$data['cart_total']	= $this->cart->format_number($this->cart->total());				
		$this->_view('checkout', $data);  		   		
	}
	
	/**
	* This method handle purchase product process and generate paypal payment form
	* @access public
	* @return 
	* @author AsifU
	*/
	function checkout_process(){		
		 
		 
		 if ($this->input->post('checkout_email')) 
		 {
			 // form data			 
			$data['phone'] = $this->input->post('phone');
			$data['company'] = $this->input->post('company');
			$data['address'] = $this->input->post('address');
			$data['order_note'] = $this->input->post('order_note');
			// check payment option
			if($this->input->post('payment_type')=='paypal_standard')
			{
				// other payment information
				$data['total_amount']		 = $this->cart->total();
				$data['user_id'] = $this->user_id;
				$data['payment_method'] = 'paypal';
				$data['payment_status'] = 'on hold';
				// insert order
				$order_id = $this->checkout_model->add_order_data($data);
				//$order_id = true;
				if($order_id)
				{
					//Set variables for paypal form
					$paypalURL = $this->config->item('paypal_url_sandbox');
					$paypalID  = $this->config->item('paypal_email'); //business email
					$returnURL = $this->config->item('return_url'); //payment success url
					$cancelURL = $this->config->item('cancel_url'); //payment cancel url
					$notifyURL = $this->config->item('ipn_url'); //ipn url		
					
					// add field to paypal form
					$this->paypal_lib->add_field('business', $paypalID);
					$this->paypal_lib->add_field('return', $returnURL);
					$this->paypal_lib->add_field('cancel_return', $cancelURL);
					$this->paypal_lib->add_field('notify_url', $notifyURL);		
					$this->paypal_lib->add_field('custom', $order_id);	 // order id	
					
					// set input fields for item name, price and quatity
					$products = $this->cart->contents();
					$i=1;
					foreach($products as $product){
						$this->paypal_lib->add_field('item_name_'.$i.'', $product['name']);
						$this->paypal_lib->add_field('item_number_'.$i.'',  $product['id']);
						$this->paypal_lib->add_field('amount_'.$i.'',  $product['price']);	
						$this->paypal_lib->add_field('quantity_'.$i.'',  $product['qty']);	
						$i++;
					}
					// generate form with all input fields
					$this->paypal_lib->paypal_auto_form();
				}
			}elseif($this->input->post('payment_type')=='paypal_express_checkout'){
				
				$this->process_express_checkout();
			}
			
			
		 }
	}
	
	/**
	* This method handle login process throught ajax
	* @access public
	* @return error or success message in JSON form
	* @author AsifU
	*/
	public function login_process()
	{
		// validate user email and password and then return result in JSON encoded form
		$validate_user = $this->checkout_model->validate_user();
		echo $validate_user;
		exit;
	}
	
	/**
	* This method add payment info with transaction id in database.
	* @access public
	* @return 
	* @author AsifU
	*/
	function success()
	{		
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();		
		//$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['total_amount'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= "completed";
		$order_id				= $paypalInfo["custom"];		
		// update order with transaction id
		$this->checkout_model->updateTransaction($data, $order_id);
		// Destroy all cart data
		$this->cart->destroy(); 
		// load success view
		$this->_view('success', $data);		
	}
	
	/**
	* This method load cancel template
	* @access public
	* @return 
	* @author AsifU
	*/
	function cancel(){		
		$data['error_msg'] = 'Something went wrong please try again.';
		if($this->input->get('token'))
		{
			$httpresponse = $this->paypal_lib->DoExpressCheckoutPayment();
			$data['error_msg'] = urldecode($httpresponse["L_LONGMESSAGE0"]);
		}				
		$this->cart->destroy(); // Destroy all cart data
		$this->_view('cancel', $data);		
	}
	
	/**
	* This function is called when transaction is completed successfully on paypal before returning to website.
	* @access public
	* @return 
	* @author AsifU
	*/
	function ipn()
	{		
		
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();		
		//$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['total_amount'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= $paypalInfo["payment_status"];
		$order_id				= $paypalInfo["custom"];
		
		$paypalURL  = $this->paypal_lib->paypal_url;		
		$result		= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
		
		//check whether the payment is verified
		if(preg_match($result, "VERIFIED")){
		    //insert the transaction data into the database
			$this->checkout_model->updateTransaction($data, $order_id);
		}

		
	}
	
	########################################### EXPRESS CHECKOUT ###############################################
	/**
	* This function 
	* @access public
	* @return 
	* @author AsifU
	*/
	public function process_express_checkout()
	{
		$products = $this->cart->contents();
		$total_amount = $this->cart->total();
		//Now you will be redirected to the PayPal to enter your customer data
		//After that, you will be returned to the RETURN URL with TOKEN and PAYER ID		
		$this->paypal_lib->SetExpressCheckOut($products, $total_amount);	
	}
	
	public function success_express_checkout(){
		if($this->input->get('token')){			
			//------------------DoExpressCheckoutPayment-------------------				
			//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
			//we will be using these two variables to execute the "DoExpressCheckoutPayment"
			//Note: we haven't received any payment yet.		
			$httpresponse = $this->paypal_lib->DoExpressCheckoutPayment();
			
			//Check if everything is fine.
			if("SUCCESS" == strtoupper($httpresponse["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpresponse["ACK"])){
				//$data['txn_id'] = urldecode($httpresponse["PAYMENTINFO_0_TRANSACTIONID"]);
				if('Completed' == $httpresponse["PAYMENTINFO_0_PAYMENTSTATUS"]){
					$data['success_msg'] = 'Payment Received! Your product will be sent to you very soon!';
				}elseif('Pending' == $httpresponse["PAYMENTINFO_0_PAYMENTSTATUS"]){
					$data['error_msg'] = 'Transaction Complete, but payment may still be pending!';
				}
			}else{
				$data['error_msg'] = urldecode($httpresponse["L_LONGMESSAGE0"]);
			}
			$orderdata['txn_id']	= urldecode($httpresponse["PAYMENTINFO_0_TRANSACTIONID"]);
			$orderdata['total_amount'] = $this->cart->total();
			$orderdata['user_id'] = $this->user_id;
			$orderdata['payment_method'] = 'paypal';
			$orderdata['payer_email'] = $this->input->get('PayerID');
			$orderdata['payment_status'] = $httpresponse["PAYMENTINFO_0_PAYMENTSTATUS"];
			// save buyer information in database
			$this->checkout_model->add_order_data($orderdata);
			$this->cart->destroy(); // Destroy all cart data
			
		}else{
			$data['error_msg'] = 'Something went wrong please try again';
		}
		// load view
		$this->_view('success', $data);
	}
	
	
	
}
/* End of file checkout.php */
/* Location: application/modules/checkout/controllers/checkout.php */