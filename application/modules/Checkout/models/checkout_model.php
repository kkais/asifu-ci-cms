<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Checkout Model Class
 *
 * Contain all methods checkout form and payment
 * @author		Asifu
 */
class Checkout_Model extends CI_Model{
	
	/**
	* This method get user data by user id
	* @access public
	* @return user data array
	* @author AsifU
	*/
	public function get_checkout_user_data( $user_id )
	{
		$query = $this->db->select('a.id,a.user_name,a.first_name,a.last_name,a.email')->where('a.id', $user_id)->get('tbl_users a');
        return $query->row();
	}
	
	/**
	* This method insert user order detail
	* @access public
	* @return bool
	* @author AsifU
	*/
	public function add_order_data($order_data)
	{
		if( !empty( $order_data ) ){
			$this->db->insert( 'orders', $order_data );
			$order_id = $this->db->insert_id();
		}
		if( $order_id!='' )
		{
			$this->add_order_items($order_id);
		}
		return (isset($order_id)) ? $order_id : FALSE;
	}
	
	/**
	* This method insert cart items in database
	* @access public
	* @return bool
	* @author AsifU
	*/
	public function add_order_items($order_id)
	{
		$cart_items = $this->cart->contents();
		foreach($cart_items as $cart_item){
			$item['name']	= $cart_item['name'];
			$item['qty']	= $cart_item['qty'];
			$item['price']	= $cart_item['price'];
			$item['subtotal']	= $cart_item['subtotal'];
			$item['product_id']	= $cart_item['id'];
			$item['order_id']	= $order_id;
			$this->db->insert( 'order_items', $item );
		}
	}
	
	/**
	* This method check if email and password exist in database or not
	* @access public
	* @return json encode result
	* @author AsifU
	*/
	public function validate_user()
	{
		$data['error'] = FALSE;
		// data array of http request		
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		
		// retrieve posted data
		$email =  $request->email; 
    	$pass = $request->pass;
		
		// query to check email in database
		$query = $this->db->select('email' . ', email, id, password')
		                  ->where('email', $email)
		                  ->limit(1)
		    			  ->order_by('id', 'desc')
		                  ->get('tbl_users');
		// if record is zero, return error				  
		if( $query->num_rows()==0 ){	
			$data['error'] = TRUE;		
			$data['msg'] = 'Email does not exist.';	
		}
		
		
		
		// if user exists, compare password
		if( $query->num_rows()==1 ){
			$user = $query->row();
			$user_id = $user->id;
			$hash_password = $user->password;
			if(!$this->bcrypt->verify($pass,$hash_password ))
			{
				$data['error'] = TRUE;
				$data['msg'] = 'Password is wrong, Please try again.';	
			}
			
		}
		
		// Email and password matches, login user and save user data in session
		if( $data['error']==FALSE ){
			$session_data = array(
			'logged_in'			   => true,
		    'email'                => $user->email,
		    'user_id'              => $user->id //everyone likes to overwrite id so we'll use user_id
			);
			$this->session->set_userdata($session_data);				
			$data['msg'] = 'Login success';
		}
		return json_encode($data);				  
	}
	
	/**
	 * This method update transaction when payment completed
	 * @access public
	 * @params payment info array
	 * @params order id to update
	 * @return bool
	 * @author Asif.u
	 **/
	public function updateTransaction($data, $id) 
	{
        $query = $this->db->where('id', $id)->update('orders', $data);
        return $query;
    }
	
	/**
	 * GetTransactionDetails get all transaction detain and save buyer information in 
	 * @access public
	 * @return bool
	 * @author Asif.u
	 **/
	public function GetTransactionDetails() 
	{
		// we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
		// GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
        $padata = 	'&TOKEN='.urlencode($this->input->get('token'));
		$httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $padata, $this->config->item('paypal_api_user'), $this->config->item('paypal_api_password'), $this->config->item('paypal_api_signature'), $this->config->item('paypal_mode'));
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
			
		}
    }
	
}
/* End of file checkout_Model.php */
/* Location: application/modules/checkout/model/checkout_Model.php */