<div class="container" id="checkout" ng-controller="checkoutCtrl" ng-init="cartTotal='<?php echo $cart_total; ?>'; base_url='<?php echo base_url(); ?>'; user_logged_in='<?php echo $logged_in; ?>'; user=<?php echo htmlspecialchars(json_encode($user)); ?>">
	<div class="col-md-9">
		<!-------- Alert box for non logged in users ----->
		<div class="alert alert-danger" ng-if="user_logged_in">
			Please <a href="" data-toggle="modal" data-target="#loginModal">click here to login</a>.
		</div>
		<!--# Alert box -->
		<div class="box">
			<!-------- validation errors ----->
			<?php if(validation_errors() != false) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo validation_errors(); ?>
				</div>
			<?php } ?>
			<!--# validation errors-->
			
			
			<!-- checkout form -->
			<form method="post" action="<?php echo base_url(); ?>checkout/checkout_process">	
				<h1>Checkout </h1>
				<div class="content">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="firstname">Firstname</label>
								<input type="text" name="first_name" class="form-control" ng-model="user.first_name" required id="first_name">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="lastname">Lastname</label>
								<input type="text" class="form-control" ng-model="user.last_name" required name="last_name" id="lastname">
							</div>
						</div>
					</div>
					<!-- /.row -->
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label for="company">Company</label>
								<input type="text" class="form-control" required name="company" id="company">
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label for="company">Email</label>
								<input type="email" class="form-control" ng-model="user.email" required name="checkout_email" id="email">
							</div>
						</div>
					</div>
					<!-- /.row -->	
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label for="address">Address</label>
								<input type="text" class="form-control" name="address" id="address">
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label for="phone">Phone</label>
								<input type="text" class="form-control" name="phone" id="phone">
							</div>
						</div>
					</div>
					<!-- /.row -->
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<textarea class="form-control" name="order_note">Special Note</textarea>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12 col-md-12"><h3>Payment Methods </h3></div>
						<?php if(!empty($payment_options)){?>
							<?php for($i=0; $i<count($payment_options); $i++){?>
								<div class="col-sm-6">
									<div class="box payment-method">
										<h4><?php echo ucwords(str_replace('_', ' ', $payment_options[$i])); ?></h4>		
										<p>We like it all.</p>		
										<div class="box-footer text-center">
											<input type="radio" name="payment_type" ng-model="payment_type" value="<?php echo $payment_options[$i]; ?>">
										</div>
									</div>
								</div>
							<?php } ?>
						<?php }else{ ?>
							<div class="alert alert-danger">Sorry, there are no available methods this time.</div>
						<?php } ?>						
					</div>
                 </div>
				<div class="box-footer">
					<div class="pull-left">
						<a href="<?php echo base_url('cart'); ?>" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to cart</a>
					</div>
					<div class="pull-right">
						<button type="submit" class="btn btn-primary" ng-disabled="user_logged_in || !payment_type">Place Order<i class="fa fa-chevron-right"></i>
						</button>
					</div>
                </div>
			</form>
		</div>
	</div>
	
	  <div class="col-md-3">

			<div class="box" id="order-summary">
				<div class="box-header">
					<h3>Order summary</h3>
				</div>
				<div class="table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td>Order subtotal</td>
								<th>${{cartTotal}}</th>
							</tr>
						   
							<tr class="total">
								<td>Total</td>
								<th>${{cartTotal}}</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

       </div>
      <!-- /.col-md-3 -->
	  <div class="modal fade" id="loginModal" role="dialog">
	  		<div class="modal-dialog">
				<!-- Modal content-->
  				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Please Login</h3>
					</div>
					
					<div class="modal-body">
						
							
						<div class="alert alert-danger" ng-if="error_login" ng-model="login_msg">{{login_msg}}</div>
						
						
						<form method="post" name="login_form" ng-submit="loginProcess(login_form.$valid)" novalidate>
							<div class="form-group" ng-class="{ 'has-error' : login_form.email.$invalid && !login_form.email.$pristine }">
                                <input class="form-control" placeholder="E-mail" ng-required="true" name="email" ng-model="email" type="email" autofocus>
								<p ng-show="login_form.email.$invalid && !login_form.email.$pristine" class="help-block">Enter a valid email.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : login_form.password.$invalid && !login_form.password.$pristine }">
                                <input class="form-control" placeholder="Password" ng-required="true" name="password" ng-model="password" type="password" value="">
								<p ng-show="login_form.password.$invalid && !login_form.password.$pristine" class="help-block">Your password is required.</p>
                            </div>
							<input type="hidden" name="redirect_to" value="{{base_url}}checkout">
							<button class="btn btn-lg btn-success btn-block" ng-disabled="login_form.$invalid" type="submit">Login</button>
						</form>
					</div><!--modal body-->
				</div>
			</div>
	  </div>
</div>
<script>
	app.directive('', function(){
		
	});
	
	//var checkouttApp = angular.module("checkoutApp", []); // controller = checkoutCtrl
	app.controller("checkoutCtrl", function($scope, $http){		
		
		
		$scope.error_login = false;	
		// function to submit the form after all validation has occurred
		$scope.loginProcess = function (isValid) {
		
			var email = $scope.email;
			var pass  = $scope.password;
			
			// Ajax http post call to save cart items in session.
			$http({
				method : "POST",
				url : $scope.base_url+'checkout/login_process',
				data : 
					{ 
						email 		: email,
						pass		: pass,
					},
			}).then(function mySucces(response) {
				// handle success message
				$scope.login_msg = response.data.msg;
				$scope.error_login = response.data.error;
				if(!$scope.error_login){
					 //alert(success);
					 window.location.reload();
				}
				console.log($scope.loginmsg);
				//$scope.cartTotal = response.data.cart_total;	
			}, function myError(response) {
				// handle error show warning			
				alert('error');			
			});
		}
		
		 
	});

</script>