<div class="container" id="checkout">
	<div class="col-md-12">		
		
		<div class="box">
			<?php if(isset($success_msg)){ ?> <div class="alert alert-success"><?php echo $success_msg; ?></div><?php } ?>
			<?php if(isset($error_msg)){ ?><div class="alert alert-danger"><?php echo $error_msg; ?></div><?php } ?>
			<p>Check your order detail <a href="<?php echo base_url('order'); ?>">here</a></p>
		</div>
	</div>
  
</div>
