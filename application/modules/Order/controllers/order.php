<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter order class for orders list and order detail
 *
 * Display of list orders and order detail template.
 * @author		Asifu
 */
class Order extends MY_Controller {
	
	var $user_id = '';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('order_model');
		$this->user_id = $this->permission->get_user_id();
	}	
	
	/**
	* This method load list of orders
	* @access public
	* @return void()
	* @author AsifU
	*/
	function index()
	{ 		
		$data['orders']		 = $this->order_model->get_orders($this->user_id);
		$this->_view('orders', $data);     		
	}
	
	/**
	* This method handle detail of order
	* @access public
	* @param order id
	* @return void()
	* @author AsifU
	*/
	function detail($order_id)
	{ 		
		$data['order_detail']		 = $this->order_model->get_order_detail($order_id);
		$data['order_amount']		 = $this->order_model->get_order_amount($order_id);
		$data['order_id']	= $order_id;
		$this->_view('detail', $data);     		
	}
	
	/**
	* My account page
	* @access public
	* @Param 
	* @return user data array in JSON form
	* @author AsifU
	*/
	public function myaccount()
	{
		if( $this->permission->is_user_logged_in() ){
			$data['user_data'] = $this->order_model->get_user_account_data($this->user_id);
			$this->_view('myaccount', $data);  
		}else{
			redirect('auth');
		}		
	}	
}
/* End of file order.php */
/* Location: application/modules/order/controllers/order.php */