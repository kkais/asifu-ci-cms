<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter CART Model Class
 *
 * Contain all methods of order related
 * @author		Asifu
 */
class Order_Model extends CI_Model{
	
	/**
	* This method get all orders by user id
	* @access public
	* @param user id
	* @return order data array
	* @author AsifU
	*/
	public function get_orders($user_id)
	{
		$query = $this->db->where('user_id', $user_id)->order_by('id', 'desc')->get('orders');
        return $query->result();
	}
	
	/**
	* This method get order detail by order id
	* @access public
	* @param order id
	* @return order data array
	* @author AsifU
	*/
	public function get_order_detail($order_id)
	{
		$query = $this->db
				->select('a.user_id,a.total_amount,a.id as order_id,b.name,b.qty,b.price,b.subtotal,c.image,c.slug')
				->join('order_items b', 'a.id = b.order_id', 'inner')
				->join('products c', 'b.product_id = c.id', 'inner')
				->where('a.id', $order_id)
				->get('orders a');
        return $query->result();
	}
	
	/**
	* This method get order total amount
	* @access public
	* @param order id
	* @return order amount
	* @author AsifU
	*/
	public function get_order_amount($order_id)
	{
		$query = $this->db->select('total_amount')->where('id', $order_id)->get('orders a');
        return $query->row();
	}
	
	/**
	* This method retrieve user account data
	* @access public
	* @param Login user id
	* @return user data array
	* @author AsifU
	*/
	public function get_user_account_data($user_id)
	{
		$query = $this->db->select('user_name, email, first_name, last_name')->where('id', $user_id)->get('tbl_users');
        return $query->row();
	}
}
/* End of file order_model.php */
/* Location: application/modules/order/model/order_model.php */