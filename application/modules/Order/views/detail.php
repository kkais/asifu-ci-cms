<div class="container" ng-controller="orderDetailCtrl" ng-init="order_items=<?php echo htmlspecialchars(json_encode($order_detail)); ?>;base_url='<?php echo base_url(); ?>'; order_id=<?php echo htmlspecialchars(json_encode($order_id)); ?>;total=<?php echo htmlspecialchars(json_encode($order_amount)); ?>">

		<div class="col-md-3">
			<!-- *** CUSTOMER MENU ***-->
			<div class="panel panel-default sidebar-menu">

				<div class="panel-heading">
					<h3 class="panel-title">Customer section</h3>
				</div>

				<div class="panel-body">

					<ul class="nav nav-pills nav-stacked">
						<li class="active">
							<a href="{{base_url}}order"><i class="fa fa-list"></i> My orders</a>
						</li>
						<li>
							<a href="{{base_url}}order/myaccount"><i class="fa fa-user"></i> My account</a>
						</li>
						<li>
							<a href="{{base_url}}auth/logout"><i class="fa fa-sign-out"></i> Logout</a>
						</li>
					</ul>
				</div>

			</div>
		</div>	
		<!-- /.col-md-3 -->
		<!-- *** CUSTOMER MENU END *** -->
		
		<!-- *** CUSTOMER ORDER DETAIL ***-->
		<div class="col-md-9" id="customer-orders">
				<div class="box">
						
                        <h1>Order #{{order_id}}</h1>
                        <p class="text-muted">If you have any questions, please feel free to <a href="#">contact us</a>, our customer service center is working for you 24/7.</p>

                        <hr>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Product</th>
                                        <th>Quantity</th>
                                        <th>Unit price</th>
                                        <th colspan="2">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in order_items">
                                        <td>
                                            <a href="#">
                                                <img width="50" src="{{base_url}}assets/images/products/{{item.image}}" alt="White Blouse Armani">
                                            </a>
                                        </td>
                                        <td><a href="#">{{item.name}}</a>
                                        </td>
                                        <td>{{item.qty}}</td>
                                        <td>{{item.price | currency}}</td>
                                        <td colspan="2">{{item.subtotal | currency}}</td>
                                    </tr> 
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5" class="text-right">Total</th>
                                        <th>{{total.total_amount | currency}}</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                        <!-- /.table-responsive -->

                        <div class="row addresses">
                            <div class="col-md-6">
                                <h2>Invoice address</h2>
                                <p>John Brown
                                    <br>13/25 New Avenue
                                    <br>New Heaven
                                    <br>45Y 73J
                                    <br>England
                                    <br>Great Britain</p>
                            </div>
                            <div class="col-md-6">
                                <h2>Shipping address</h2>
                                <p>John Brown
                                    <br>13/25 New Avenue
                                    <br>New Heaven
                                    <br>45Y 73J
                                    <br>England
                                    <br>Great Britain</p>
                            </div>
                        </div>

                    </div>
		</div>				
		<!-- *** CUSTOMER ORDER DETAIL END *** -->	
		
</div>
<script>
app.controller("orderDetailCtrl", function($scope, $http){
	
});
</script>