<div class="container" ng-controller="myAccountCtrl" ng-init="user_data=<?php echo htmlspecialchars(json_encode($user_data)); ?>;base_url='<?php echo base_url(); ?>'">

		<div class="col-md-3">
			<!-- *** CUSTOMER MENU ***-->
			<div class="panel panel-default sidebar-menu">

				<div class="panel-heading">
					<h3 class="panel-title">Customer section</h3>
				</div>

				<div class="panel-body">

					<ul class="nav nav-pills nav-stacked">
						<li>
							<a href="{{base_url}}order"><i class="fa fa-list"></i> My orders</a>
						</li>
						<li class="active">
							<a href="{{base_url}}order/myaccount"><i class="fa fa-user"></i> My account</a>
						</li>
						<li>
							<a href="{{base_url}}auth/logout"><i class="fa fa-sign-out"></i> Logout</a>
						</li>
					</ul>
				</div>

			</div>
		</div>	
		<!-- /.col-md-3 -->
		<!-- *** CUSTOMER MENU END *** -->
		
		<!-- *** CUSTOMER ORDER DETAIL ***-->
		<div class="col-md-9" id="customer-orders">
				<div class="box">
						
                        <h1>My Account</h1>
                        <h4>
						Hello {{user_data.first_name}} (not {{user_data.first_name}}? <a href="{{base_url}}auth/logout">Sign out</a>). From your account dashboard you can view your recent orders.
						</h4>    
                        <!-- /.table-responsive -->

                        

                    </div>
		</div>				
		<!-- *** CUSTOMER ORDER DETAIL END *** -->	
		
</div>
<script>
app.controller("myAccountCtrl", function($scope, $http){
	
});
</script>