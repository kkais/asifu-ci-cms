<div class="container" ng-controller="orderListCtrl" ng-init="orders=<?php echo htmlspecialchars(json_encode($orders)); ?>;base_url='<?php echo base_url(); ?>'">

		<div class="col-md-3">
			<!-- *** CUSTOMER MENU ***-->
			<div class="panel panel-default sidebar-menu">

				<div class="panel-heading">
					<h3 class="panel-title">Customer section</h3>
				</div>

				<div class="panel-body">

					<ul class="nav nav-pills nav-stacked">
						<li class="active">
							<a href="{{base_url}}order"><i class="fa fa-list"></i> My orders</a>
						</li>
						<li>
							<a href="{{base_url}}order/myaccount"><i class="fa fa-user"></i> My account</a>
						</li>
						<li>
							<a href="{{base_url}}auth/logout"><i class="fa fa-sign-out"></i> Logout</a>
						</li>
					</ul>
				</div>

			</div>
		</div>	
		<!-- /.col-md-3 -->
		<!-- *** CUSTOMER MENU END *** -->
		
		<!-- *** CUSTOMER ORDERS ***-->
		<div class="col-md-9" id="customer-orders">
				<div class="box">
					<h1>My orders</h1>

					<p class="lead">Your orders on one place.</p>
					<p class="text-muted">If you have any questions, please feel free to <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p>

					<hr>
					
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Order</th>
									<th>Date</th>
									<th>Total</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="order in orders">
									<th>#{{order.id}}</th>
									<td>{{order.order_date | date : "dd.MM.y"}}</td>
									<td>{{order.total_amount | currency}}</td>
									<td><span ng-class="order.payment_status=='Completed'?'label label-success':'label label-warning'">{{order.payment_status}}</span>
									</td>
									<td><a href="{{base_url}}order/detail/{{order.id}}" class="btn btn-primary btn-sm">View</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
		</div>				
		<!-- *** CUSTOMER ORDERS END *** -->	
		
</div>
<script>

app.controller("orderListCtrl", function($scope, $http){
	//$scope.formatDate = function(date){
          //var orderDate = new Date(date);
          //return orderDate;
		//  console.log(date);
   // };
});
</script>