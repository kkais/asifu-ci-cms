<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Brand controller Class
 *
 * Enable admin to manage categories
 * @author		Asifu
 */
class Brand extends Admin_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('products/product_model');
		$this->load->model('brand_model');
		$this->load->helper(array('form', 'slug', 'common'));
		$this->load->library(array('upload', 'form_validation'));
	}
	
	/**
	* This method list all brands in admin
	* @access public
	* @return void()
	* @author AsifU
	*/	
	public function index()
	{		
		$brands = $this->brand_model->get_all_brands(); // get all categories		
		// save data in array		
		$data['brands'] = $brands;
		// load view
		$this->_view('admin/brands_list.php', $data);
	}
	
	/**
	* This method create brand
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function add(){
		if($this->input->post('add_brand'))
		{
			$error = FALSE;
			//validate input field
			$this->form_validation->set_rules( 'name', 'Name', 'required' );
			//input data
			$data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');		
			//if no errors process form			
			if ($this->form_validation->run() === TRUE)
			{
				if(!$error){
					$id = $this->brand_model->insert($data);
					redirect('admin/brands');				
				}
			}
		}
			
		// load view
		$this->_view('admin/add_brand.php');
	}
	
	/**
	* This method delete brand
	* @access public
	* @params brand id
	* @return void()
	* @author AsifU
	*/	
	public function delete($id) 
	{	
        $this->brand_model->delete($id);
        redirect('/admin/brands/', 'refresh');
    }
	
	/**
	* This method handle edit brand process
	* @access public
	* @params $brand_id int
	* @return void()
	* @author AsifU
	*/
	public function edit($brand_id=NULL) 
	{
		if ($this->input->post('edit_brand')) 
		{
			// validate input field
            $this->form_validation->set_rules( 'name', 'Name', 'required' );
			//input data
			$data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');			
			
			if ($this->form_validation->run() === TRUE)
			{
					        
				$this->brand_model->update($data, $brand_id);				
				redirect('/admin/brands', 'refresh');
				
			}
        }
		$category	  = $this->brand_id->get_brand($brand_id); // 
		$data['brand'] = $category;
        $this->_view('admin/edit_brand', $data);
	}
	
}
/* End of file brand.php */
/* Location: application/modules/products/controllers/brand.php */