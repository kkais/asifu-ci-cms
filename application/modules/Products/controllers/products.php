<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Products controller Class
 *
 * Enable admin to manage products
 * @author		Asifu
 */
class Products extends Admin_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('brand_model');
		$this->load->model('category/category_model');
		$this->load->helper(array('form', 'slug', 'common'));
		$this->load->library(array('upload', 'form_validation'));
	}
	
	/**
	* This method list all products in admin
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function index()
	{		
		$all_products = $this->product_model->get_all_products(); // get all products		
		// save data in array		
		$data['products'] = $all_products;
		// load view
		$this->_view('admin/products_list.php', $data);
	}
	
	/**
	* This method create product
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function add(){
		
		if($this->input->post('add_product'))
		{
			// validate input field
			$error = false;
			$this->form_validation->set_rules( 'name', 'Name', 'required' );
			$this->form_validation->set_rules( 'price', 'Price', 'trim|numeric|required' );
			// input data
			$data['name'] = $this->input->post('name');
            $data['price'] = $this->input->post('price');
            $data['description'] = $this->input->post('description');
			$data['created_at'] = date("Y-m-d H:i:s");
			
            $cat_id 	   = $this->input->post('category'); 
			$data['brand_id'] = $this->input->post('brand'); 
			// product slug
			$slug	= create_slug($data['name']);
			// check if product slug exist or not			
			$baseSlug = $slug;
			$i=1;
			while($this->product_model->product_slug_exist($slug)){
				$slug = $baseSlug . "-" . $i++;
			}
			$data['slug'] 	= $slug;	
			$this->load->library('image_lib');
			if(!empty($_FILES['userFile']['name']))
			{
				$filesCount = count($_FILES['userFile']['name']);				
				foreach($_FILES as $key=>$value)
				{
					for($i = 0; $i < $filesCount; $i++)
					{
						//initialize all images variables
						$_FILES['userFile']['name'] = $value['name'][$i];				
						$_FILES['userFile']['type'] = $value['type'][$i];
						$_FILES['userFile']['tmp_name'] = $value['tmp_name'][$i];
						$_FILES['userFile']['error'] = $value['error'][$i];
						$_FILES['userFile']['size'] = $value['size'][$i];
						
						if($i==0) $data['image'] = $_FILES['userFile']['name'];
						// upload path of product images
						$uploadPath = 'assets/images/products/';
						
						// configuration of images
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						//$config['max_size']	= '100';
						//$config['max_width'] = '1024';
						//$config['max_height'] = '768';
						
						// initialize upload library with new configurations
						$this->upload->initialize($config);
	
						if($this->upload->do_upload('userFile'))
						{
							$fileData = $this->upload->data();
							// create array of all images data
							$uploadData[$i]['img_name'] = $fileData['file_name'];
							$uploadData[$i]['created'] = date("Y-m-d H:i:s");
							$uploadData[$i]['modified'] = date("Y-m-d H:i:s");							
							// resize image to 150x150
							$this->resize($uploadPath,$uploadData[$i]['img_name']);							
						}
						
						
					} // for loop end
				} // endforeach
			} // endif
					
			// if no errors process form			
			if ($this->form_validation->run() === TRUE){
				if(!$error){
					$id = $this->product_model->insert($data, $cat_id, $uploadData);
					redirect('admin/products');
				}
			}

		}		
		$data['brands']	  	  = $this->brand_model->get_all_brands(); // get all brands for dropdown
		$categories	  = $this->product_model->get_categories_dropdown();
		$data['categories'] = $categories; // save data in array
		// load view
		$this->_view('admin/add_product.php', $data);
	}
	
	/**
	* This method handle edit product process
	* @access public
	* @params $path = path to product folder
	* @params $file = path of image name to resize
	* @return void()
	* @author AsifU
	*/
	public function resize($path, $file){
		$this->load->library('image_lib');
		$config['image_library'] = 'gd2';
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 150;
		$config['height']       = 150;	
		$config['source_image'] = $path.$file;
		$this->image_lib->clear();
       	$this->image_lib->initialize($config);
       	$this->image_lib->resize();
		return;
	}
	
	/**
	* This method handle edit product process
	* @access public
	* @params $product_id int
	* @return void()
	* @author AsifU
	*/
	public function edit( $product_id ) {
		if ($this->input->post('update_product')) {
            $error = false;
			$this->form_validation->set_rules( 'name', 'Name', 'required' );
			$this->form_validation->set_rules( 'price', 'Price', 'trim|numeric|required' );
			// input data
			$data['name'] = $this->input->post('name');
            $data['price'] = $this->input->post('price');
            $data['description'] = $this->input->post('description');
			$data['brand_id'] = $this->input->post('brand'); 
			$data['image'] = $this->input->post('image'); 
			$data['image_id'] = $this->input->post('image_id'); 
			// product slug
			$slug	= create_slug($data['name']);
			// check if product slug exist or not			
			$baseSlug = $slug;
			$i=1;
			while($this->product_model->product_slug_exist($slug)){
				$slug = $baseSlug . "-" . $i++;
			}
			$data['slug'] 	= $slug;	
			
			$this->load->library('image_lib');
			$uploadData = array();
			if(!empty($_FILES['userFile']['name']))
			{
				$filesCount = count($_FILES['userFile']['name']);				
				foreach($_FILES as $key=>$value)
				{
					for($i = 0; $i < $filesCount; $i++)
					{
						//initialize all images variables
						$_FILES['userFile']['name'] = $value['name'][$i];				
						$_FILES['userFile']['type'] = $value['type'][$i];
						$_FILES['userFile']['tmp_name'] = $value['tmp_name'][$i];
						$_FILES['userFile']['error'] = $value['error'][$i];
						$_FILES['userFile']['size'] = $value['size'][$i];
						
						if($i==0) $data['image'] = $_FILES['userFile']['name'];
						
						// upload path of product images
						$uploadPath = 'assets/images/products/';
						
						
						
						// configuration of images
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						
						// initialize upload library with new configurations
						$this->upload->initialize($config);
						
						if($this->upload->do_upload('userFile'))
						{
							$fileData = $this->upload->data();
							// create array of all images data
							$uploadData[$i]['img_name'] = $fileData['file_name'];
							$uploadData[$i]['created'] = date("Y-m-d H:i:s");
							$uploadData[$i]['modified'] = date("Y-m-d H:i:s");							
							// resize image to 150x150
							$this->resize($uploadPath,$uploadData[$i]['img_name']);							
						}
					}
				}
			}
			
			/*echo '<pre>';
			print_r($uploadData);
			echo '</pre>';
			exit;*/
			
			if ($this->form_validation->run() == TRUE)
			{
				if(!$error){			        
					$this->product_model->update($data,$product_id,$uploadData);				
					$category['category_id'] =  $this->input->post('category'); 
					$this->product_model->update_category( $category, $product_id );
					
					redirect('/admin/products', 'refresh');
				}
			}
        }
		$data['brands']	= $this->brand_model->get_all_brands(); // get all brands for dropdown
		$products 	  = $this->product_model->get_product( $product_id );
		$categories	  = $this->category_model->get_all_categories(); // get all categories for dropdown
		$product_images = $this->product_model->get_product_images( $products->image_id );
		//print_r($product_images); exit;
		$data['product'] = $products;
		$data['product_categories'] = $categories;
		$data['images'] = $product_images;
        $this->_view('admin/edit_product', $data);
	}
	
	public function delete( $id ) {
        $this->product_model->delete( $id );
        redirect('/admin/products', 'refresh');
    }
	
	/**
	* This method handle image upload process
	* @access public
	* @params 
	* @return errors and on success it return image data
	* @author AsifU
	*/	
	public function do_upload()
	{
		$config = array(
			'upload_path' => 'assets/images/products/',
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "1700",
			'max_width' => "2500"
		);
		$this->upload->initialize($config);
		if($this->upload->do_upload())
		{
			$data = array('upload_data' => $this->upload->data());
			return $data;
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			///$this->form_validation->run() = 0;
			return $error;
		}
	}
	
	
	
}
/* End of file Products.php */
/* Location: application/modules/products/controllers/products.php */