<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter brand model Class
 *
 * Enable admin to manage brands
 * @author		Asifu
 */
class brand_Model extends CI_Model{
	
	var $iteration_number = 0;
	
	/**
	* This method Retrieve All brands
	* @access public
	* @return array with all brands
	* @author AsifU
	*/
	public function get_all_brands()
	{
		$query = $this->db->get('brands');
		return $query->result();
		//echo "hiii";
	}
		
	/**
	 * This funtion insert brand
	 * @access public
	 * @params brand data array
	 * @return bool
	 * @author Asif.u
	 **/
	public function insert($brand_data)
	{
		
		if( !empty( $brand_data ) )
		{
			$this->db->insert( 'brands', $brand_data );
			$id = $this->db->insert_id();
		}
		return (isset($id)) ? $id : FALSE;
	}
	
	
	/**
	 * This method delete brand
	 * @access public
	 * @params brand id
	 * @return bool
	 * @author Asif.u
	 **/
	public function delete($id) 
	{		
		$this->db->where('id', $id);
		$this->db->delete('brands');          
	}
	
	/**
	 * This method retrive brand datat by brand id
	 * @access public
	 * @params brand id
	 * @return array of brand data
	 * @author Asif.u
	 **/
	public function get_brand($id) 
	{		
		$query = $this->db->where('id', $id)->get('brands');
		return $query->row();
	}
	
	/**
	 * This method update brand by brand id
	 * @access public
	 * @params brand data array
	 * @params product id
	 * @return bool
	 * @author Asif.u
	 **/
	public function update($data, $id) {
        $query = $this->db->where('id', $id)->update('brands', $data);
        return $query;
    }
	
	

}
/* End of file brand_model.php */
/* Location: application/modules/products/model/brand_model.php */