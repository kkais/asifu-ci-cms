<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Products Model Class
 *
 * Contain all methods of product
 * @author		Asifu
 */
class Product_Model extends CI_Model{
	
	var $iteration = 0;
	
	/**
	* This method Retrieve All products
	* @access public
	* @params 
	* @return array with all products
	* @author AsifU
	*/
	public function get_all_products(){
		$query = $this->db
				->select('a.id,a.name,a.price,a.created_at,a.image,a.image_id, b.product_id,a.slug,a.brand_id, b.category_id,
					 c.name as cat_name,c.cat_parent')
				->join('products_categories b', 'a.id = b.product_id', 'left')
				->join('categories c', 'b.category_id = c.id', 'left')
				->get('products a');	  
				  	
        return $query->result();
	}
	
	/**
	 * This funtion insert product
	 * @access public
	 * @params product data array
	 * @params category id, can be null
	 * @return bool
	 * @author Asif.u
	 **/
	public function insert( $product_data, $category_id=NULL, $uploadData=NULL ){
		
		if( !empty( $product_data ) ){
			$addImages = $this->insert_images($uploadData);
			if($addImages){
				$image_ids = implode(',', $addImages);
				$product_data['image_id'] = $image_ids;
			}
			$this->db->insert( 'products', $product_data );
			$id = $this->db->insert_id();
		}
		if( $category_id!='' ){
			$this->add_product_cat( $id, $category_id );
		}
		
		return (isset($id)) ? $id : FALSE;
	}
	
	/**
	 * This funtion insert product images
	 * @access public
	 * @params image data array
	 * @return last insert id
	 * @author Asif.u
	 **/
	public function insert_images($image_data)
	{
		if(count($image_data)>0)
		{
			$image_id = array();
			for($i=0; $i<count($image_data); $i++){
				$this->db->insert( 'images', $image_data[$i] );
				$id = $this->db->insert_id();
				$image_id[] = $id;
			}
		}	
		return (count($image_id)>0) ? $image_id : FALSE;
	}
	
	/**
	 * This funtion retrieve all images for product
	 * @access public
	 * @params image_id (multiple image ids seperated by commas)
	 * @return images data
	 * @author Asif.u
	 **/
	 function get_product_images($image_ids)
	 {
		 $qry = "SELECT * FROM images where id IN(".$image_ids.")";
		 $res	= $this->db->query( $qry );
		 $result = $res->result();
		 return $result;
		 
	 }
	
	/**
	 * This method update product
	 * @access public
	 * @params product data array
	 * @params product id
	 * @return bool
	 * @author Asif.u
	 **/
	public function update($data, $id, $uploadData) {
		if(count($uploadData)>0){
			$this->delete_image($data['image_id']);			
			$addImages = $this->insert_images($uploadData);
			if($addImages){
				$image_ids = implode(',', $addImages);
				$data['image_id'] = $image_ids;
			}
		}	
		$query = $this->db->where('id', $id)->update('products', $data);
		return $query;
    }
	
	/**
	 * This method update product category
	 * @access public
	 * @params category data array
	 * @params product id
	 * @return bool
	 * @author Asif.u
	 **/
	public function update_category($data, $product_id) 
	{		
		$query = $this->db->where('product_id', $product_id)->get('products_categories');
		if($query->num_rows()>0){
        	$query = $this->db->where('product_id', $product_id)->update('products_categories', $data);
		}else{
			$this->add_product_cat($product_id, $data['category_id']);
		}
        return $query;
    }
	
	/**
	 * This funtion create relationship between categories and product.
	 * @access public
	 * @params product id
	 * @params cat_id id
	 * @return bool
	 * @author Asif.u
	 **/
	public function add_product_cat($product_id, $cat_id)
	{
		$cat_data = array( 'product_id' => $product_id, 'category_id' => $cat_id );
		$this->db->insert( 'products_categories', $cat_data );
	}
	
	/**
	 * This method get product by product id
	 * @access public
	 * @params product id
	 * @return array of product data
	 * @author Asif.u
	 **/
	public function get_product( $product_id )
	{			
		$query = $this->db->select('a.id,a.name,a.price,a.created_at,a.description,a.image,a.brand_id,a.image_id, b.product_id, b.category_id')
		->join('products_categories b', 'a.id = b.product_id', 'left')
		->where('a.id', $product_id)
		->get('products a');
        return $query->row();
	}
	
	/**
	 * This method get product by product slug
	 * @access public
	 * @params product slug
	 * @return array of product data
	 * @author Asif.u
	 **/
	public function get_product_by_slug( $product_slug )
	{			
		$query = $this->db->select('a.id,a.name,a.price,a.created_at,a.description,a.image,a.image_id, b.product_id, b.category_id')
		->join('products_categories b', 'a.id = b.product_id', 'left')
		->where('a.slug', $product_slug)
		->get('products a');
        return $query->row();
	}
	
	/**
	 * This method delete product and also delete data from related table
	 * @access public
	 * @params product id
	 * @return bool
	 * @author Asif.u
	 **/
	public function delete($product_id) {
		$query = "DELETE a, b FROM products a
				  INNER JOIN products_categories b ON a.id = b.product_id
				  WHERE a.id = ".$product_id."";
		$res	= $this->db->query( $query );
		return $res;		
        
	}
	
	/**
	 * This method delete all images
	 * @access public
	 * @params image_id
	 * @return bool
	 * @author Asif.u
	 **/
	public function delete_image($image_id) {
		$query = "DELETE FROM images WHERE id IN(".$image_id.")";
		$res	= $this->db->query( $query );
		return $res;		
        
	}	
	
	/**
	* This method checked for the slug in database
	* @access public
	* @params slug
	* @return bool
	* @author AsifU
	*/
	function product_slug_exist($slug) 
	{
		// query to retrieve slug
		$query = $this->db->where('slug', $slug)->get('products');
		return $query->num_rows();
	}
	
	/**
	* This method create categories and subcategories tree dropdown
	* @access public
	* @param int parent id
	* @return dropdown html for all options
	* @author AsifU
	*/
	function get_categories_dropdown($parent = 0) {
		$this->iteration++;
		$query = $this->db->where('cat_parent', $parent)->get('categories');
		$first_level = $query->result();        
        foreach ($first_level as $fl) {
			$indent = '';
			$query = $this->db->where('cat_parent', $fl->id)->get('categories');
            $count = $query->num_rows();
			if($fl->cat_parent>0) $indent = '-';
            if ($count != 0) {
               @$tree .= '<option value="'.$fl->id.'">'.$indent.$fl->name.'</option>';
               @$tree .= $this->get_categories_dropdown($fl->id);
            }else {
                @$tree .= '<option value="'.$fl->id.'">'.$indent.$fl->name.'</option>';
            }
        }
        return $tree;
	}
}
/* End of file product_model.php */
/* Location: application/modules/products/model/product_model.php */