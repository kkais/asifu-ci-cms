<script src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Products
                <a  href="<?= base_url('admin/products') ?>" class="btn btn-warning">Go back to products</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
			<?php if(validation_errors() != FALSE  || $this->session->flashdata('message') == TRUE ) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo validation_errors(); ?>
					<!--- validation used for image upload-->
					<?php if ($this->session->flashdata('message')): ?>
						<?= $this->session->flashdata('message') ?>
					<?php endif; ?>
				</div>
			<?php } ?>
			
				
			
			<!----EOF image upload errors-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add New Product
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form method="POST" action="<?= base_url('admin/products/add/') ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" placeholder="Name" name="name">
                                </div>
								<div class="form-group">
                                    <label>Price</label>
                                    <input class="form-control" placeholder="Price" name="price">
                                </div>
								<div class="form-group">
																		
								<select name="category" class="form-control">	
									<option value="">Select Category</option>
									<?php echo $categories; ?>
								</select>			
								</div>
								<div class="form-group">
									
								<select name="brand" class="form-control">	
									<option value="">Select Brand</option>
									<?php foreach($brands as $brand):?>
									<option value="<?php echo $brand->id; ?>"><?php echo $brand->name; ?></option>
									<?php endforeach; ?>
								</select>			
								</div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
								<div class="form-group">
									<label>Image Upload</label>
									<input type="file" name="userFile[]" multiple>
								</div>
								<input type="hidden" name="add_product" value="1">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>