<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Brands
                    <a  href="<?php echo base_url('admin/brand/add')?>" class="btn btn-success">Add new</a>
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
					
            <div class="panel panel-default">
                <div class="panel-heading">
                    Brands
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
									<th>Description</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($brands)): ?>
								
                                    <?php foreach ($brands as $key => $list): ?>
									
                                        <tr class="odd gradeX">
                                            <td><?php echo $list->id; ?></td>
                                            <td><?php echo $list->name; ?></td>
                                            <td><?php echo $list->description; ?></td>
                                            
                                            
                                            <td>
                                                
                                                <a href="<?php echo base_url('admin/brand/delete/'.$list->id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this product?');">delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="even gradeC">
                                        <td>No data</td>
                                        <td>No data</td>                                        
                                        <td>No data</td>
                                        <td>
                                            <a href="#" class="btn btn-danger">delete</a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                            <tfooter>
                                <tr>
                                  	<th>ID</th>
                                    <th>Name</th>
									<th>Description</th>
									<th>Action</th>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>
<!-- /#page-wrapper -->
