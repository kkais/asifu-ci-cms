<script src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Products
                <a  href="<?= base_url('admin/products') ?>" class="btn btn-warning">Go back to products listing</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
			<?php if(validation_errors() != FALSE  || $this->session->flashdata('message') == TRUE ) { ?>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<?php echo validation_errors(); ?>
					<!--- validation used for image upload-->
					<?php if ($this->session->flashdata('message')): ?>
						<?= $this->session->flashdata('message') ?>
					<?php endif; ?>
				</div>
			<?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Products
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?= base_url('admin/products/edit/' . $product->id) ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="<?= $product->name ?>" class="form-control" placeholder="Name" name="name">
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input value="<?= $product->price; ?>" class="form-control" placeholder="Price" id="price" name="price">
                                </div>
								<div class="form-group">
                                    <label>Description</label>
									<textarea name="description"><?php echo $product->description?></textarea>
                                </div>
                                
								<input type="hidden" name="update_product" value="1">
                               
                                <div class="form-group">
                                    <label>Categories</label>
                                    <select class="form-control" id="category" name="category">
                                        <?php if (count($product_categories)): ?>
                                            <?php foreach ($product_categories as $key => $product_category): ?>
                                                <option value="<?= $product_category->id; ?>" <?= ($product->category_id == $product_category->id) ? 'selected="selected"' : '' ?>> <?= $product_category->name ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
								
								<div class="form-group">
									
                                    <label>Brands</label>
                                    <select class="form-control" id="category" name="brand">
										<option value="">Select brand</option>
                                        <?php if (count($brands)): ?>
                                            <?php foreach ($brands as $brand): ?>
                                                <option value="<?= $brand->id; ?>" <?= ($product->brand_id == $brand->id) ? 'selected="selected"' : '' ?>> <?= $brand->name ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                
								<div class="form-group">
									<label>Image Upload</label>
									
									<input type="file" name="userFile[]" multiple>
									<input type="hidden" name="image" value="<?php echo $product->image; ?>">
									<input type="hidden" name="image_id" value="<?php echo $product->image_id; ?>">
								</div>
								<div class="form-group">
									<?php if(!empty($images)): foreach($images as $file): ?>
										<span class="item">
										<img width="80" width="80" src="<?php echo base_url('assets/images/products/'.$file->img_name); ?>" alt="" >
										</span>
									<?php endforeach; else: ?>
									 	<p>Image(s) not found.....</p>
									<?php endif; ?>
								</div>
								
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>