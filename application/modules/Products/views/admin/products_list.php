<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Products
                    <a  href="<?php echo base_url('admin/products/add')?>" class="btn btn-success">Add a new</a>
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
					
            <div class="panel panel-default">
                <div class="panel-heading">
                    Products listing
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Category</th>
									<th>Date</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($products)): ?>
								
                                    <?php foreach ($products as $key => $list): ?>
									
                                        <tr class="odd gradeX">
                                            <td><?php echo $list->id; ?></td>
                                            <td><?php echo $list->name; ?></td>
                                            <td><?php echo $list->price; ?></td>
                                            <td><?php if(!empty($list->cat_name)){ echo $list->cat_name; }else{ echo '--'; } ?></td>
                                            <td><?php echo $list->created_at; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('admin/products/edit/'.$list->id); ?>" class="btn btn-info">edit</a>  
                                                <a href="<?php echo base_url('admin/products/delete/'.$list->id); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this product?');">delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="even gradeC">
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>
                                            <a href="#" class="btn btn-info">edit</a>  
                                            <a href="#" class="btn btn-danger">delete</a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                            <tfooter>
                                <tr>
                                   <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Category</th>
									<th>Date</th>
									<th>Action</th>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>
<!-- /#page-wrapper -->
