<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter template options class
 *
 * Enable admin to manage different options of website
 * @author		Asifu
 */
class Settings extends Admin_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form'));
		$this->load->model('setting_model');
	}
	
	/**
	* This 
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function index()
	{				
		
	}
	
	/**
	* This method load payment setting form to display all payment options
	* @access public
	* @return void()
	* @author AsifU
	*/	
	public function payment()
	{	
		$option		   = 'payment_options';
		$data['payment_options'] = $this->setting_model->get_option($option);
		$this->_view('payment_setting', $data);
	}
	
	/**
	* This method handle payment settings form process
	* @access public
	* @return void()
	* @author AsifU
	*/	
	public function add()
	{	
		if($this->input->post('add_payment_option')) 
		{
			$option		   = 'payment_options';
			// array of different payment options
			$payment_value = $this->input->post('payment_options');
			$payment_options = $this->setting_model->get_option($option);	
			if($payment_options === FALSE){
				$this->setting_model->add_option($option, $payment_value);
			}else{
				$this->setting_model->update_option($option, $payment_value);												
			}
			
		}
		redirect('admin/settings/payment');			
	}
	
	
	
	
	
}
/* End of file Settings.php */
/* Location: application/modules/products/controllers/settings.php */