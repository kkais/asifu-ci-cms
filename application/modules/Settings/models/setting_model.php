<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Setting Model Class
 *
 * Contain methods for all options for website
 * @author	Asifu
 */
class Setting_Model extends CI_Model{
	
	/**
	* This add options to database
	* @access public
	* @params $option is option name
	* @params $value is option value
	* @return bool
	* @author AsifU
	*/
	public function add_option($option, $value){
		$option = trim($option);
		// check if option key is empty
		if(empty($option)) return FALSE;
		// if option value is in array form then run serialize
		if(is_array($value) || is_object($value)) 
			$value = serialize($value);				
		// insert options	
		$result = $this->db->query("INSERT INTO `options` (option_name, option_value) VALUES ('".$option."', '".$value."')");
		if(!$result) 
			return FALSE;	
		// return true if no error		
		return TRUE;
	}
	
	/**
	* This retrieve option by option key
	* @access public
	* @params $option is the option name
	* @return option value
	* @author AsifU
	*/
	public function get_option($option){
		$query = $this->db->where('option_name', $option)->get('options');		
		$result = $query->row();
		if(!$result)
			return FALSE;
		if($this->is_serialized($result->option_value))
			return @unserialize($result->option_value);
		if(empty($result->option_value)) return '';	
		return $result->option_value;	
	}
	
	/**
	* update option value for specific option key 
	* @access public
	* @params 
	* @return 
	* @author AsifU
	*/
	public function update_option($option, $value){
		$option = trim($option);
		// check if option key is empty
		if(empty($option)) 
			return FALSE;
		// if option value is in array form then run serialize
		if(is_array($value) || is_object($value)) 
			$value = serialize($value);			
					
		$result = $this->db->query("UPDATE `options` SET option_value='".$value."' WHERE option_name = '".$option."' ");
		if(!$result) 
			return FALSE;	
		// return true if no error		
		return TRUE;
	}
	
	
	/**
	* This method checked if option value is serialized or not (function copied from wordpress)
	* @access public
	* @params $data is option value
	* @return bool
	* @author AsifU
	*/
	function is_serialized( $data ) 
	{
		// if it isn't a string, it isn't serialized
		if ( !is_string( $data ) )
			return false;
		$data = trim( $data );
		if ( 'N;' == $data )
			return true;
		if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
			return false;
		switch ( $badions[1] ) {
			case 'a' :
			case 'O' :
			case 's' :
				if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
					return true;
				break;
			case 'b' :
			case 'i' :
			case 'd' :
				if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
					return true;
				break;
		}
		return false;
	}
	
	
}
/* End of file setting_model.php */
/* Location: application/modules/settings/model/setting_model.php */