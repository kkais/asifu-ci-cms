<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Payment Settings
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
	
			<!-- payment form -->
			 
			<form method="POST" action="<?= base_url('admin/settings/add/') ?>">
					<div class="form-group">
						<input type="checkbox" <?php if(!empty($payment_options)){ if(in_array('paypal_standard', $payment_options)){ ?> checked <?php }} ?> name="payment_options[]" value="paypal_standard">
						<span>Enable Paypal Standard</span>                                    
					</div>
					<div class="form-group">
						<input type="checkbox" name="payment_options[]" <?php if(!empty($payment_options)){ if(in_array('paypal_express_checkout', $payment_options)){ ?> checked <?php }} ?> value="paypal_express_checkout">
						<span>Enable Paypal Express Checkout</span>                                    
					</div>
					<input type="hidden" name="add_payment_option" value="1">
					<button type="submit" class="btn btn-primary">Save Changes</button>
          	</form>
            
            <!-- /.payment form -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>