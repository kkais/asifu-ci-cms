<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter shop class for products and categories
 *
 * Display of shop page and categories templates
 * @author		Asifu
 */
class Shop extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('products/product_model', 'category/category_model', 'shop_model','products/brand_model'));
	}
	
	/**
	* This method list all products on frontend
	* @access public
	* @params 
	* @return void()
	* @author AsifU
	*/	
	public function index()
	{				
		$all_products = $this->product_model->get_all_products(); // get all products
		$brands		  = $this->brand_model->get_all_brands(); // get all products
		$categories   = getTreeAngular(); // get all categories	
		
		// save data in array		
		$data['products'] = json_encode($all_products);
		
		$data['categories'] = json_encode($categories);
		$data['brands'] 	= json_encode($brands);
		
		// load view
		$this->_view('shop', $data);
	}
	
	/**
	* This method send all products to ajax request in Angularjs
	* @access public
	* @return all products in json
	* @author AsifU
	*/	
	public function get_products_json()
	{				
		// data array of http request		
		 $postdata = file_get_contents("php://input");
		 $request = json_decode($postdata);
		 
		 
    	 // Validate posted data, and then add the item!		 
		 $start =  $request->start; // Assign posted product_id to $id
    	 $limit = $request->limit; // Assign posted quantity to $cty	
		 
		$all_products = $this->shop_model->get_all_products($start, $limit); // get all products
		
		$product_length = count($this->product_model->get_all_products()); //get product length
		
		// save data in array		
		$data['product_length'] = $product_length;
		$data['products'] = $all_products;
		
		// load view
		echo json_encode($data);
		exit;
	}
	
	/**
	* This method view detail page of products
	* @access public
	* @params product slug
	* @return void()
	* @author AsifU
	*/	
	public function detail($slug)
	{
		// product detail		
		$product_detail = $this->product_model->get_product_by_slug($slug); 
		// product images
		if( !empty( $product_detail->image_id )){
			$product_images = $this->product_model->get_product_images($product_detail->image_id);			
		}else{
			$product_images = 0;
		}
		// save data in array		
		$data['product'] = $product_detail;
		$data['product_images'] = $product_images;
		
		
		// load view
		$this->_view('detail', $data);
	}
	
	/**
	* This method list product by parent and child category
	* @access public
	* @params parent category
	* @params child category
	* @return void()
	* @author AsifU
	*/	
	public function category($parent_cat, $child_cat='')	
	{	
		$slug = ($child_cat!='')?$child_cat:$parent_cat;
		$products = $this->shop_model->get_shop_category_products($slug); // product detail
		// save data in array		
		$data['products'] = $products;
		// load view
		$this->_view('shop', $data);
	}
	
	
}
/* End of file shop.php */
/* Location: application/modules/shop/controllers/shop.php */