<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Shop Model Class
 *
 * Contain all methods shop related
 * @author		Asifu
 */
class Shop_Model extends CI_Model{
	
	/**
	* This method Retrieve specific category products
	* @access public
	* @params category slug
	* @return array of category products
	* @author AsifU
	*/
	public function get_shop_category_products($slug){
		$query = $this->db
				->select('a.id,a.name,a.price,a.created_at,a.image, b.product_id,a.slug,a.brand_id, b.category_id, c.name as cat_name')
				->join('products_categories b', 'a.id = b.product_id', 'left')
				->join('categories c', 'b.category_id = c.id', 'left')
				->where('c.slug', $slug)
				->get('products a');
        return $query->result();
	}
	
	/**
	* This method retrieve limited products for lazy load
	* @access public
	* @params $start = 0
	* @params $limit = 10
	* @return array with all products
	* @author AsifU
	*/
	public function get_all_products($start=0, $limit=10){
		$query = "SELECT a.id,a.name,a.price,a.created_at,a.image, b.product_id,a.slug,a.brand_id, b.category_id, c.name as cat_name,c.cat_parent 
				  FROM products a
				  LEFT JOIN products_categories b ON a.id = b.product_id
				  LEFT JOIN categories c ON b.category_id = c.id
				  LIMIT ".$start.",".$limit."";
		 $result = $this->db->query($query);		  
         return $result->result();
	}
	
}
/* End of file shop_model.php */
/* Location: application/modules/shop/model/shop_model.php */