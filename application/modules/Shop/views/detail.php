<script>
//(function(){
	app.controller("prodDetailCtrl", function($scope, $rootScope, $http, cartItems){
		$scope.productData = <?php echo json_encode($product); ?>;
		// global variable for cart quantity
		$rootScope.cartCounter = cartItems;
	});
//});
</script>
 <div class="container">
	 <div class="col-md-12" ng-controller="prodDetailCtrl" ng-init="base_url='<?php echo base_url(); ?>'; images=<?php echo htmlspecialchars(json_encode($product_images)); ?>">
	 	<div class="row" id="productMain">
			<div class="col-sm-6">
				<div id="mainImage">
					<img src="{{base_url}}assets/images/products/{{productData.image}}" alt="" class="img-responsive">
				</div>
				
			</div>
			
            <div class="col-sm-6">
                <div class="box">
					<h1 class="text-center">{{productData.name}}</h1>
					
					<p ng-bind-html="productData.description"></p>	
					
					<p class="price">{{productData.price | currency}}</p>
					<!--<p class="text-center buttons">
						<a href="" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Add to cart</a> 
					</p>-->
                </div>  
				<div class="row" id="thumbs">
				
					<div class="col-xs-4" ng-repeat="image in images">
						<a href="{{base_url}}assets/images/products/{{image.img_name}}" class="thumb">
							<img src="{{base_url}}assets/images/products/{{image.img_name}}" alt="" class="img-responsive">
						</a>
					</div>					
					
                </div>			           
             </div>
         </div>
	 </div>
 </div><!--container-->
