<div class="container" id="products_container"  ng-controller="prodListCtrl" ng-init="products = <?php echo htmlspecialchars($products); ?>; base_url='<?php echo base_url(); ?>'">
		
	<div class="col-md-9">
		<div class="row products">
		
	<div class="box info-bar">
		<div class="row">
			<div class="col-sm-12 col-md-6 products-showing">
				Showing <strong>4</strong> of <strong>{{products.length}}</strong> products
			</div>		
			<div class="col-sm-12 col-md-6  products-number-sort">
				<div class="row">
				<form class="form-inline">								
					<div class="products-sort-by pull-right">
							<strong>Sort by</strong>
							<select name="sort-by" class="form-control" ng-model="sortBy">
								<option value="">Default sorting</option>
								<option value="price">Sort by price: low to high</option>
								<option value="price-desc">Sort by price: high to low</option>
							</select>
					</div>				
				</form>
				</div>
			</div>
		</div>
	</div>
		
		<div class="alert alert-success" id="item_added_msg" ng-if="showsuccess">{{successmsg}}</div>
		<div class="alert alert-danger" ng-if="showerror">{{errormsg}}</div>
		<div infinite-scroll='nextPage()' infinite-scroll-disabled='busy' infinite-scroll-distance='1' style="height: 1200px">
		<!-- products' listing-->
		<div class="col-md-4 col-sm-4" ng-repeat="product in products | searchFor:searchString:this | shopFilter:sortBy:this | 
		filterByCat:filterCatId:this | rangeFilter:range:this | filterByBrand:pbrands:this" ng-if="products.length > 0" style="min-height:580px;">
				<form class="add_to_cart_form" method="post" action="#">
				<input type="hidden" name="product_id" value="{{product.id}}">
				<input type="hidden" name="max_qty" value="2">
				<input type="hidden" name="quantity" value="1">
				<div class="product">
					<div class="flip-container">
						<div class="flipper">
							<div class="front">
								<a href="{{base_url}}shop/detail/{{product.slug}">
									<img src="{{base_url}}assets/images/products/{{product.image!=''?product.image:'noimage.jpg'}}" alt="" class="img-responsive">
								</a>
							</div>
							<div class="back">
								<a href="{{base_url}}shop/detail/{{product.slug}}">
									<img src="{{base_url}}assets/images/products/{{product.image!=''?product.image:'noimage.jpg'}}" alt="" class="img-responsive">
								</a>
							</div>
							
						</div>
					</div>
					<a href="{{base_url}}shop/detail/{{product.slug}}" class="invisible">
						<img src="{{base_url}}assets/images/products/{{product.image}}" alt="" class="img-responsive">
					</a>
					<div class="text">
						<h3><a href="{{base_url}}shop/detail/{{product.slug}}">{{product.name}}</a></h3>
						<p class="price">{{product.price | currency}}</p>
						<p class="buttons">
						<a href="{{base_url}}shop/detail/{{product.slug}}" class="btn btn-default">View detail</a>						
							<button name="add_to_cart" type="button" ng-click="addToCart(product)" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</button>
							
						</p>
						
					</div>
					
				</div>
				</form>
			</div>
			<div ng-show="busy">Loading data...</div>
		</div><!---infinite scroll-->	
			
			<!--# products' listing-->
		<div class="col-md-3 col-sm-4" ng-if="products.length==0 || products_not_found==1" >
			<p>No products found.</p>
		</div>
		
		
		<div class="pages col-md-12 col-sm-12">
		<!--<uib-pagination total-items="products.length" items-per-page="itemsPerPage" ng-model="currentPage" ng-change="pageChanged()" boundary-links="false">
		</uib-pagination>-->
		</div>
	
	</div><!-- products main -->
	</div>
	
	
	<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
	<div class="col-md-3">
		<!--- Brands -->
		<div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Brands <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox" ng-repeat="brand in brands">
                                        <label>
                                            <input type="checkbox" checked name="pbrands1[]" value="{{brand.id}}" ng-model="pbrands[brand.id]">{{brand.name}} 
                                        </label>
                                    </div>
                                </div>
                            </form>
							
							

                        </div>
                    </div>
		
					
		<!--- CATEGORIES AND SUBCATEGORIES TREE -->
		<div class="panel panel-default sidebar-menu">
				<div class="panel-heading">
					<h3 class="panel-title">Filter by categories <a class="btn btn-xs btn-danger pull-right" ng-click="clearAll()" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
				</div>				
				<div class="panel-body" id="cat-model" dynamic="categories"></div>				
		</div>
		<!-- EOF TREE -->
		
		
		
		<div class="panel panel-default sidebar-menu">
			<div class="panel-heading">
				<h3 class="panel-title">Filter by price</h3>
			</div>
			<div class="panel-body">			
				<div range-slider min="minPrice" max="maxPrice" model-min="range.min" model-max="range.max"></div>
				<span class="pull-right">Price: {{range.min | currency}} — {{range.max | currency}}</span>
				
			</div>
		</div>
    </div>
	
</div>
<script>
// define angular module/app for product listing

/**
* CONTROLLER FOR PRODUCT LISTING ITEMS
*/
(function(){

	app.controller("prodListCtrl", function($scope, $sce, $rootScope, $http, cartItems){	
	
	
	$scope.pbrands = {};
	
	// initialize pagination variables
  	$scope.currentPage = 1; // pagination start from first page
	$scope.itemsPerPage = 5; // Maximum number of products per page
	$scope.busy = false; // hide loading data div
	
	// categories,brands and products array
	$scope.categories = '<?php echo getTreeAngular(); ?>';
	$scope.products = <?php echo $products; ?>;
	$scope.brands = <?php echo $brands; ?>;
	
	// total product
	$scope.total_products = $scope.products.length;
	// find total pages for pagination based on items per page
	$scope.total_pages	  = parseInt(Math.ceil($scope.total_products/$scope.itemsPerPage));
	
	
	// initialize start and limit variables
	$scope.begin = (($scope.currentPage - 1) * $scope.itemsPerPage); // start variable
	$scope.end = $scope.begin + $scope.itemsPerPage; // Limit to 
	
	// initialize array for products
	var limitedProducts = [];
	
	// This method trigger when you scroll the page
	$scope.nextPage = function () {				
		if ($scope.busy) return;
		// if no more prodcuts return it
		if($scope.total_pages<=0) return;
		// decrement total pages on every ajax call
		$scope.total_pages--;
		// show loader
		$scope.busy = true;
		// Ajax request for products
		$http({
			method : "POST",
			url : 'http://localhost/ci-cms/shop/get_products_json?start='+$scope.begin+"&limit="+$scope.end,
			data : 
				{ 
					start 	: $scope.begin,
					limit	: $scope.end
				},
		}).then(function mySucces(response) {				
				angular.forEach(response.data.products, function(item){	
					if(limitedProducts.indexOf(item)==-1){	 // only unique data allowed
						limitedProducts.push(item); // push result to new array
					}
				});
				$scope.products = limitedProducts;
				$scope.busy = false;
				$scope.currentPage++;	
				$scope.begin = (($scope.currentPage - 1) * $scope.itemsPerPage); // start variable
			//	console.log($scope.begin);
				//$scope.end   = 	$scope.begin + $scope.itemsPerPage; // Limit to  				
		});
	}	
	// call nextpage method on page load	 
	$scope.nextPage();	
	
	// get min price from products for price range slider
	$scope.minPrice = Math.min.apply(Math,$scope.products.map(function(item){return item.price;}));
	// get max price from products for price range slider
	$scope.maxPrice = Math.max.apply(Math,$scope.products.map(function(item){return item.price;}));	
	// just set price range for slider
	$scope.range = {
		min: 0,
		max: $scope.maxPrice
	};	
		
	// global variable for cart quantity
	$rootScope.cartCounter = cartItems;
	
	// initialize pagination variables
	$scope.maxSize = 5; // Limit number for pagination size.

	// create a watch function to observe and monitor currentPage variable
	$scope.$watch('currentPage + itemsPerPage', function() {
    	var begin = (($scope.currentPage - 1) * $scope.itemsPerPage); // start variable
    	var end = begin + $scope.itemsPerPage; // Limit to  
    	$scope.filterProducts = $scope.products.slice(begin, end);
  	});
	
		$scope.products_not_found = 0;
		
		// This function is trigger when add to cart button is clicked
		$scope.addToCart = function (val) {
		// Ajax http post call to save cart items in session.
		$http({
			method : "POST",
			url : $scope.base_url+'cart/add_cart_item',
			data : 
				{ 
					product_id 	: val.product_id,
					quantity	: 1,
					max_qty		: 2
				},
		}).then(function mySucces(response) {
			$scope.success = response.data.success;
			if($scope.success=='success'){
				$rootScope.cartCounter = response.data.cart_items;
				$scope.showsuccess = true; // display cart div
				$scope.successmsg = response.data.success_msg; // cart message text 
				jQuery("html, body").animate({ scrollTop: 0 }, "slow");
			}
		}, function myError(response) {
			// handle error show warning			
			$scope.showerror = true;
			$scope.errormsg  = 'Something went wrong please try again.';
			jQuery("html, body").animate({ scrollTop: 0 }, "slow");
		});
		//return false;		
    }	
	
		// clear all checkboxes
		$scope.clearAll = function(){
			$scope.filterCatId = [];
		}
	});
	
	// Create the instant search by keyword
	app.filter('searchFor', function(){		
		// All filters must return a function. The first parameter is the data that is to be filtered,
		// the second is an argument that may be passed with a colon (searchFor:searchString)
		return function(arr,searchString,scope){
			
			// if no search term return same result
			if(!searchString){
				return arr;
			}
			
			//initialize array
			var result = [];
			// transform search terms to lowercase
			searchString = searchString.toLowerCase();
			// Search keyword in all products
			// Using the forEach helper method to loop through the all products
			angular.forEach(scope.products, function(item){
				// if search term exist in product title then push it to array
				if(item.name.toLowerCase().indexOf(searchString) !== -1){
					result.push(item);
				}
			});
			
			// check if searched products exist or not.
			if(result.length==0){ 
				scope.products_not_found=1;  
			}else{
				scope.products_not_found=0;  
			}
			return result;
		}
	});
	
	// Create custom filter for product price filter
	app.filter('shopFilter', function(){
		return function(arr,sortBy,scope){		
			// if no value is selected from dropdown return all products			
			if(!sortBy){
				return arr;
			}	
			
			var result = []
			result = arr;
			//sort method sort the products of an array
			// return array either asending or descending order
			result.sort(function(a, b){
				a = parseInt(a['price']);
				b = parseInt(b['price']);
				if(sortBy=='price-desc') return b-a; // high to low price
				return a - b; // Low to high price
			});
			
			// return filtered results
			return result;			
		}
	});
	
	app.filter('filterByBrand', function(){
		return function(arr,brandObj,scope){
			
			if(isEmpty(brandObj)){
				return arr;
			}
		//	console.log(brandObj);
			// By default no brands are selected		
			var all_checked = false;	
			
			var result1 = [];
			// loop through all checked categories
			angular.forEach(brandObj, function(key,value){
				//if checkbox is checked
				if(key==true){	
					// loop through all product to filter checked brands products only
					angular.forEach(arr, function(item){		
						if(result1.indexOf(item)==-1){									
							if(item.brand_id.indexOf(value) !== -1){
								result1.push(item); // push result to new array
							}
						}
					});
					// one of the checkbox is checked
					all_checked = true;
				}
			});
			
			scope.products_not_found=0;  
			
			// if unchecked all checkboxes return all products
			if(!all_checked) return arr;
			
			// if no products found for checked category show message
			if(result1.length==0){ 
				scope.products_not_found=1;  
			}
			
			// return new filtered result
			return result1;
			
		}
	});
	
	// create custom filter products by categories
	app.filter('filterByCat', function(){
		
		return function(arr,catObj,scope){
			
			if(!catObj){
				return arr;
			}
			console.log(catObj);
			// By default no categories are selected		
			var all_checked = false;	
						
			//initialize result array
			var result = [];
			
			// loop through all checked categories
			angular.forEach(catObj, function(key,value){
				//if checkbox is checked
				if(key==true){	
					// loop through all product to filter checked category products only
					angular.forEach(arr, function(item){		
						if(result.indexOf(item)==-1){									
							if(item.category_id.indexOf(value) !== -1 || item.cat_parent.indexOf(value) !== -1){
								result.push(item); // push result to new array
							}
						}
					});
					// one of the checkbox is checked
					all_checked = true;
				}
			});
			
			//console.log(result);
			
			scope.products_not_found=0;  
			
			// if unchecked all checkboxes return all products
			if(!all_checked) return arr;
			
			// if no products found for checked category show message
			if(result.length==0){ 
				scope.products_not_found=1;  
			}
			
			// return new filtered result
			return result;
		}
	});
	
	
	
	// create custom filter for price range slider
	app.filter('rangeFilter', function(){
		return function(arr,rangeObj,scope){
			//if(scope.range.min)
			//console.log(scope.products);
			//console.log("###############");
			///console.log(arr);
			if(isEmpty(rangeObj)) return arr;
			scope.products_not_found=0; 
			//initialize result array
			var result = []
			// get current min and max prices
			var min = parseInt(rangeObj.min);
        	var max = parseInt(rangeObj.max);
			// Loop through all products to get products in between min and max price
			angular.forEach(arr,function(item){
				if( item.price >= min && item.price <= max ) {
					result.push(item);
				}
			});
			//console.log(result);
			//if no products in between show message
			if(result.length==0){ 
				scope.products_not_found=1; 
				return; 
			}
		//	console.log("AAAA");
			// return new filtered products
			return result;
		}
	});
	
	// custom directive to compile dynamic html of categories hierarchy
	app.directive('dynamic', function ($compile) {
	  return {
		restrict: 'A',
		replace: true,
		link: function (scope, ele, attrs) {
		  scope.$watch(attrs.dynamic, function(html) {
			ele[0].innerHTML = html;
			$compile(ele.contents())(scope);
		  });
		}
	  };
	});
	
	function isEmpty(obj){     
		for(var prop in obj) {
			if(obj.hasOwnProperty(prop))
			 	return false;     
		}     
		return true; 
	}	

})();
</script>

