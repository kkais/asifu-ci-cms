<?php

/**
 * @group Controller
 */

use PHPUnit\Framework\TestCase;
class Home_Test extends TestCase
{
	public function testExpectFooActualFoo()
    {
        $this->expectOutputString('foo');
        print 'foo';
    }

    public function testExpectBarActualBaz()
    {
        $this->expectOutputString('bar');
        print 'baz';
    }
}
