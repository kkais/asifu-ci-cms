<script src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Pages
                <a  href="<?= base_url('admin/pages') ?>" class="btn btn-warning">Go back to pages</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit user
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?= base_url('admin/pages/edit/' . $page_data->id) ?>">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" value="<?php echo $page_data->page_title; ?>" placeholder="Name" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"><?php echo $page_data->page_content; ?></textarea>
                                </div>                                
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>