<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Roles <a href="<?= base_url('admin/roles/add') ?>" class="btn btn-warning">Add New Role</a> <a href="<?= base_url('admin/roles/addPermission') ?>" class="btn btn-success">Add permission</a> 
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Roles And Capabilities
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="dataTable_wrapper">
						
							<form role="form" method="POST" action="<?= base_url('admin/roles/updateCap/') ?>">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<?php if (count($roles)): ?>	 
                                <tr>
									<th>Permission Key</th>
									<?php foreach ( $roles as $role ): ?>
										<th><?= $role->name ?></th>
									<?php endforeach; ?>
                                </tr>
									
								<?php endif; ?>
                            </thead>
							<tbody>
							<?php if ( count( $permissions ) ): ?>
								<?php foreach ( $permissions as $permission ): ?>
									<tr>
									<td><?= $permission->permission_key ?></td>
									
										<?php foreach ( $roles as $role ): ?><td>
										<?php $checked = permission_exist($role->role_id, $permission->permission_key); ?>
										<input <?php if($checked){ echo 'checked="checked"'; } ?> type="checkbox" name="capability[<?php echo $role->name; ?>][<?php echo $permission->id; ?>]" value="<?php echo $permission->permission_key; ?>"></td><?php endforeach; ?>										
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
							
							</tbody>
							</table>
							<button type="submit" class="btn btn-primary">Save Changes</button>
							</form>
						</div>	
                      


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>