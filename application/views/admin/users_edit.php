<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2>
                Users
                <a  href="<?= base_url('admin/users') ?>" class="btn btn-warning">Go back to users listing</a>
            </h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit user
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" method="POST" action="<?= base_url('admin/users/edit/' . $user->id) ?>">
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input value="<?= $user->user_name ?>" class="form-control" placeholder="User Name" name="user_name">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input value="<?= $user->first_name ?>" class="form-control" placeholder="First name" id="first_name" name="first_name">
                                </div>
								<div class="form-group">
                                    <label>Last Name</label>
                                    <input value="<?= $user->last_name ?>" class="form-control" placeholder="First name" id="first_name" name="last_name">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input value="<?= $user->email ?>" class="form-control" placeholder="Email" id="email" name="email">
                                </div>
                               
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" id="role_id" name="role_id">
                                        <?php if (count($user_roles)): ?>
                                            <?php foreach ($user_roles as $key => $user_role): ?>
                                                <option value="<?= $user_role->role_id; ?>" <?= ($user->role_id == $user_role->role_id) ? 'selected="selected"' : '' ?>> <?= $user_role->name ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                
                                
                                
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </form>
                        </div>


                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>