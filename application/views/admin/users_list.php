<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header users-header">
                <h2>
                    Users
                    <a data-toggle="modal" data-target="#registerModal"  href="<?= base_url('admin/users/create') ?>" class="btn btn-success">Add a new</a>
                </h2>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
					<?php if(validation_errors() != false) { ?>
					<div class="alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?php echo validation_errors(); ?>
					</div>
					<?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Users listing
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>User Name</th>
									<th>User Role</th>
									<th>Registration date</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($users)): ?>
								
                                    <?php foreach ($users as $key => $list): ?>
									
                                        <tr class="odd gradeX">
                                            <td><?php echo $list->id; ?></td>
                                            <td><?php echo $list->first_name." ".$list->last_name; ?></td>
                                            <td><?php echo $list->email; ?></td>
                                            <td><?php echo $list->user_name; ?></td>
											<td><?php echo $list->role_title; ?></td>
                                            <td><?php echo $list->created_on; ?></td>
                                            <td>
                                                <a href="<?= base_url('admin/users/edit/'.$list->id) ?>" class="btn btn-info">edit</a>  
                                                <a href="<?= base_url('admin/users/delete/'.$list->id) ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this user?');">delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="even gradeC">
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>No data</td>
                                        <td>
                                            <a href="#" class="btn btn-info">edit</a>  
                                            <a href="#" class="btn btn-danger">delete</a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>User Name</th>
									<th>User Role</th>
                                    <th>Registration Date</th>
                                    <th>Action</th>
                                </tr>
                            </tfooter>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
</div>
<!-- /#page-wrapper -->
<!------ Registration modal ---->
<div id="registerModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
			<div class="modal-body" style="background-color: #fff;">
				<div class="panel-heading">
                    <h3 class="panel-title">Please Register</h3>
                </div>
				<div class="panel-body">
					<form accept-charset="utf-8" method="post" action="<?php echo base_url('/admin/users/create/'); ?>">
                        <fieldset>
							<div class="form-group">
								<input type="text" placeholder="First Name" class="form-control" required id="first_name" value="" name="first_name">
							</div>
							<div class="form-group">
								<input type="text" placeholder="Last Name" required class="form-control" id="last_name" value="" name="last_name">
							</div>
							<div class="form-group">
								<input type="text" placeholder="User Name" required class="form-control" id="user_name" value="" name="user_name">
							</div>
							<div class="form-group">
								<input type="email" placeholder="Email" required class="form-control" id="email" value="" name="email">
							</div>
							<div class="form-group">
								<input type="password" placeholder="Password" required class="form-control" id="password" value="" name="password">
							</div>
							
							<div class="form-group">
								<input type="password" required placeholder="Confirm Password" class="form-control" id="confirm_password" value="" name="confirm_password">
							</div>
                            <div class="form-group">
								<?php $class = 'class="form-control"'; ?>								
								<?php echo form_dropdown('user_role', $roles,'', $class); ?>
							</div>
										
							<button type="submit" class="btn btn-lg btn-success btn-block" value="" name="submit">Register</button>
                            
                        </fieldset>                    </form>
				</div>
			</div>
		
	</div>
</div>