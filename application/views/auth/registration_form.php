<style>
.form-group p{ color:#F00; }
</style>
<div class="container" ng-controller="registerCtrl" ng-init="base_url='<?php echo base_url(); ?>'">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Register</h3>
                </div>
                <div class="panel-body">
					
					<?php if(validation_errors() != false) { ?>
					<div class="alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?php echo validation_errors(); ?>
					</div>
					<?php } ?>
                  <!-- <form class="" action="/auth/create_user" novalidate>-->
				   <form class="" action="<?php echo base_url(); ?>auth/create_user" method="post" name="registerForm" ng-submit="submitForm(registerForm)" novalidate>
                       	<fieldset>
							 <div class="form-group" ng-class="{'has-error':registerForm.first_name.$invalid && !registerForm.first_name.$pristine}">
                                <input class="form-control" placeholder="First Name" name="first_name" id="first_name" ng-model="first_name" type="text" ng-required="true">
								<p ng-show="registerForm.first_name.$invalid && !registerForm.first_name.$pristine">First name is required field.</p>
                            </div>
							
							<div class="form-group" ng-class="{'has-error':registerForm.last_name.$invalid && !registerForm.last_name.$pristine}">
								<input class="form-control" placeholder="Last Name" ng-model="last_name" name="last_name" id="last_name" type="text" ng-required="true">
								<p ng-show="registerForm.last_name.$invalid && !registerForm.last_name.$pristine">Last name is required field.</p>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="User Name" ng-model="user_name" name="user_name" id="user_name" type="text" ng-required="true">
							</div>
							<div class="form-group" ng-class="{'has-error':registerForm.email.$invalid && !registerForm.email.$pristine}">
								<input class="form-control" placeholder="Email" ng-model="email" name="email" id="email" type="email" ng-required="true" ng-blur="validateEmail(email);">
								<p ng-model="emailErrorMsg" ng-show="registerForm.email.$invalid && !registerForm.email.$pristine">{{emailErrorMsg}}</p>
								
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" ng-model="password" name="password" id="password" type="password" ng-required="true">
							</div>
							
							<div class="form-group" ng-class="{'has-error':registerForm.confirm_password.$invalid && !registerForm.confirm_password.$pristine}">
								<input class="form-control" placeholder="Confirm Password" ng-model="confirm_password" name="confirm_password" id="confirm_password" type="password" ng-required="true" password-match>
								<p ng-show="registerForm.confirm_password.$invalid && !registerForm.confirm_password.$pristine">Confirm password does not match.</p>
								
							</div>
                        
							<input type="hidden" name="user_role" id="user_role" value="2">
							<button class="btn btn-lg btn-success btn-block" ng-disabled="registerForm.$invalid" type="submit">Register</button>                            
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	// Custom directive for password match.
	app.directive('passwordMatch', function() {
		return {
			require: 'ngModel',
			link: function($scope, element, attr, registerCtrl) {
				function passwordValidation(cpass) {
					// check if password match with confirm password					
					if(cpass!=$scope.password){
						// change the validity state to false that pass does not match
						registerCtrl.$setValidity('passMatch', false);
					}else{
						// change the validity state to true and hide message
						registerCtrl.$setValidity('passMatch', true);
					}
					return cpass;
				}
				// Add passwordValidation function to an array of other functions
				registerCtrl.$parsers.push(passwordValidation);
			}
   		};
	});
	
	// Controller for registration form
	app.controller('registerCtrl', function($scope, $http){		
		$scope.emailErrorMsg = 'Please enter valid email address.';
		// this function trigger for email validation
		// @email = bool		
		$scope.validateEmail = function(emailId){
			
			// if email id is valid and return true	
			if(emailId){
				// Ajax http post call to save cart items in session.
				$http({
					method : "POST",
					url : $scope.base_url+'auth/validate_registration_form',
					data : 
						{ 
							user_email 		: $scope.email,
						},
				}).then(function mySucces(response) {
					// handle response data
					$scope.emailErrorMsg = response.data.error_msg; 
					// display error message
					$scope.error 	  = response.data.error;
					if($scope.error){
						// change the validity state to false if email exist
						$scope.registerForm.email.$setValidity('emailExist', false);  
					}
				
					if(!$scope.error){
						// change the validity state to false if email exist
						$scope.registerForm.email.$setValidity('emailExist', true);  
					}
				}, function myError(response) {
					// handle error show warning			
					console.log('error');
				});
			}
		}
		
		$scope.submitForm = function(form){
			if(form.$valid) {
				//window.location = $scope.base_url+'auth/create_user';
			}
		};
		
	});
</script>