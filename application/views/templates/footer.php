
  <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-6">
                    <p class="pull-left">© 2016 Datumsquare. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right"> <a href="#">Developed by Asif U</a> 
                        <!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
                    </p>
                </div>
            </div>
        </div>
<!-- *** COPYRIGHT END *** -->
</div><!--- all --->
<!-- jQuery -->
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js"></script>

<!-- Custom js -->
<script src="<?=base_url()?>assets/js/custom.js"></script>



<!-- Bootstrap Core JavaScript -->
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
<script src="<?=base_url()?>assets/js/waypoints.min.js"></script>
<script src="<?=base_url()?>assets/js/modernizr.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-hover-dropdown.js"></script>
<script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script>
<script src="<?=base_url()?>assets/js/front.js"></script>
</body>

</html>
