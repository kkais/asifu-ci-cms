<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title><?php if($page_title){ echo $page_title; } ?></title>

<!-- Fonts CSS -->
<link href="<?=base_url()?>assets/css/font-awesome.css" rel="stylesheet">

<!-- Bootstrap Core CSS -->
<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

<!-- For animation CSS -->
<link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">

<!-- Slider CSS -->
<link href="<?=base_url()?>assets/css/owl.carousel.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="<?=base_url()?>assets/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

<!-- CUSTOM CSS -->
<link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet">

<!-- Load CSS for angular price range slider -->
<link href="<?=base_url()?>assets/css/angular.rangeSlider.css" rel="stylesheet">

<!-- Load CSS  -->
<link href="<?=base_url()?>assets/css/angular.rangeSlider.css" rel="stylesheet">

<script src="<?=base_url()?>assets/js/respond.min.js"></script>

<!-- jQuery -->
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js"></script>

<!-- Load Angular js -->
<script src="<?= base_url() ?>assets/js/angular.min.js"></script>

<!-- Load Pricerange slider js -->
<script src="<?= base_url() ?>assets/js/angular.rangeSlider.js"></script>

<!-- Bootstrap components library written in pure AngularJS -->
<script src="<?= base_url() ?>assets/js/ui-bootstrap-tpls-0.12.1.min.js"></script>

<!-- Load Angular Route js -->
<script src="<?= base_url() ?>assets/js/angular-route.min.js"></script>

<!-- Load Angular sanitize library for html type varibles -->
<script src="<?= base_url() ?>assets/js/angular-sanitize.js"></script>

<!-- Load infinite scroll library -->
<script src="<?= base_url() ?>assets/js/ng-infinite-scroll.js"></script>

<!-- Load image magnifier library -->
<script src="<?= base_url() ?>assets/js/ng-magnify.js"></script>



<!-- Load Angular Apps js -->
<!--<script src="<?= base_url() ?>assets/js/myapp.js"></script>-->

<!-- Load Angular controllers js -->
<!--<script src="<?= base_url() ?>assets/js/myctrl.js"></script>-->
	
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
	// define angular module/app for cart counter
	
	/*var cartCounter = angular.module('cartCounter', []);
	var products = angular.module('products', []);
	var app = angular.module('myapp', ['cartCounter', 'products']);*/
	
	
	var app = angular.module("myapp", ['ui.bootstrap','ui-rangeSlider','ngSanitize','infinite-scroll']);	
	app.controller("cartCounterCtlr", function($scope, $rootScope, cartItems){	
		$rootScope.cartCounter = cartItems;
		//console.log($scope.cartCounter);
	});
	
	// creating custom service(cartQuantity) for cart counter to access and update it from other controllers
	/*app.service('cartItems', function(){
		return this.cartqty = '<?php echo $this->cart->total_items(); ?>'
	});*/
	
	app.factory('cartItems', function(){
		return cartqty = '<?php echo $this->cart->total_items(); ?>'
	});
	
</script>

<?php
// check for html header
if ($html_header) {
	echo $html_header;
}
?>
</head>
<body ng-app="myapp">
    <!-- *** TOPBAR ***
 _________________________________________________________ -->
    <div id="top">
        <div class="container">			
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
					<?php if(is_user_logged_in()):?>
						<li>
						<a href="<?php echo base_url('auth/logout'); ?>">Logout</a>
						</li>
						<li><a href="<?php echo base_url('order'); ?>">My Account</a>
						</li>
					<?php else: ?>
						<li>
						<a href="<?php echo base_url('auth'); ?>">Login</a>
						</li>
						<li><a href="<?php echo base_url('auth/register'); ?>">Register</a>
						</li>
					<?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Customer login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="customer-orders.html" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email-modal" placeholder="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password-modal" placeholder="password">
                            </div>

                            <p class="text-center">
                                <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>

                        </form>

                        <p class="text-center text-muted">Not registered yet?</p>
                        <p class="text-center text-muted"><a href="register.html"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p>

                    </div>
                </div>
            </div>
        </div>

    </div><!-- *** TOP BAR END *** -->
	
	 <!-- *** NAVBAR ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="<?php echo base_url(); ?>" data-animate-hover="bounce">
                    <img src="<?php echo base_url(); ?>assets/img/allshore-logo.png" alt="Obaju logo" class="hidden-xs">
                    <img src="<?php echo base_url(); ?>assets/img/logo-small.png" alt="Obaju logo" class="visible-xs"><span class="sr-only">Obaju - go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
                </div>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-left">
                    <li class="<?php if($this->uri->segment(1)=="" || $this->uri->segment(1)=="home"){echo "active";}?>" ><a href="<?php echo base_url(); ?>">Home</a>
                    </li>
					<li class="<?php if($this->uri->segment(1)=="shop"){echo "active";}?>"><a href="<?php echo base_url('shop'); ?>">Shop</a>
                    </li>
                    <!--<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Men <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5>Clothing</h5>
                                            <ul>
                                                <li><a href="category.html">T-shirts</a>
                                                </li>
                                                <li><a href="category.html">Shirts</a>
                                                </li>
                                                <li><a href="category.html">Pants</a>
                                                </li>
                                                <li><a href="category.html">Accessories</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                           <!-- </li>
                        </ul>
                    </li>-->
				

                    <!--<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Ladies <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h5>Clothing</h5>
                                            <ul>
                                                <li><a href="category.html">T-shirts</a>
                                                </li>
                                                <li><a href="category.html">Shirts</a>
                                                </li>
                                                <li><a href="category.html">Pants</a>
                                                </li>
                                                <li><a href="category.html">Accessories</a>
                                                </li>
                                            </ul>
                                        </div>
                                      
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            <!--</li>
                        </ul>
                    </li>-->

                    
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <div class="navbar-collapse collapse right" id="cart-overview" ng-controller="cartCounterCtlr">
                    <a href="<?php echo base_url(); ?>cart/" class="btn btn-primary navbar-btn" ng-model="cartCounter.cartqty"><i class="fa fa-shopping-cart"></i><span class="hidden-sm">{{cartCounter}} items in cart</span></a>
                </div>
                <!--/.nav-collapse -->

                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" ng-model="searchString" placeholder="Search">
                        <span class="input-group-btn">

			<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

		    </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->
	<div id="all">
		<div id="content">
			
		<?php
			// use this to display the body of the page.
			if ($page_html) {
				echo $page_html;
			}
		?>
		</div><!--content--->
	
