jQuery(document).ready(function(e) {
	var template_link = "/ci-cms";
	jQuery(".add_to_cart_form11").submit(function(e) {
		// Get the product ID and the quantity 
		var id = jQuery(this).find('input[name=product_id]').val();
		var qty = jQuery(this).find('input[name=quantity]').val();
		var max_qty = jQuery(this).find('input[name=max_qty]').val();
		jQuery.ajax({
			url : template_link+'/cart/add_cart_item',
			type : 'post',
			data : {
					product_id 	: id,
					quantity	: qty,
					max_qty		: max_qty
			},
			success: function( response ){
				if(response=='success'){
					jQuery("#item_added_msg").show().html('New product has been added to your cart.');
					jQuery("html, body").animate({ scrollTop: 0 }, "slow");
				}					
			}					
		});

		return false; // Stop the browser of loading the page defined
	});
});